#!/bin/sh

progs="old-leaf old-all-leaf new-leaf new-leaf-byname new-all-leaf new-all-leaf-byname old-node old-all-node new-node new-all-node new-all-node-byname"
allnewleaf="new-all-leaf new-all-leaf-byname"
allnewnode="new-all-node new-all-node-byname"

echo "cleaning..."
rm -f ${progs} *.txt

echo "compiling..."
for p in $progs
do
	cc -I/usr/local/include -I../../ ${p}.c -L/usr/local/lib -o ${p} -lsysctlmibinfo
done

echo "getting leaves... "

echo "old-leaf - new-leaf "
./old-leaf >> old-leaf.txt
./new-leaf >> new-leaf.txt
diff old-leaf.txt new-leaf.txt | diffstat -m
#meld old-leaf.txt new-leaf.txt
rm old-leaf.txt new-leaf.txt

echo "old-leaf - new-leaf-byname "
./old-leaf >> old-leaf.txt
./new-leaf-byname >> new-leaf-byname.txt
diff old-leaf.txt new-leaf-byname.txt | diffstat -m
rm old-leaf.txt new-leaf-byname.txt

./old-all-leaf >> old-all-leaf.txt
for l in $allnewleaf
do
	echo "old-all-leaf - ${l}"
	./${l} >> ${l}.txt
	diff old-all-leaf.txt ${l}.txt | diffstat -m
	#meld old-all-leaf.txt ${l}.txt
	rm ${l}.txt
done
rm -f old-all-leaf.txt

echo "getting nodes... "

echo "old-node - new-node"
./old-node >> old-node.txt
./new-node >> new-node.txt
diff old-node.txt new-node.txt | diffstat -m
#meld old-node.txt new-node.txt
rm old-node.txt new-node.txt

./old-all-node >> old-all-node.txt
for n in $allnewnode
do
	echo "old-all-node - ${n}"
	./${n} >> ${n}.txt
	diff old-all-node.txt ${n}.txt | diffstat -m
	#diff -y -W 120 old-all-node.txt ${n}.txt 
	#meld old-all-node.txt ${n}.txt
	rm ${n}.txt
done
rm -f old-all-node.txt

echo "cleaning..."
rm -f ${progs}

