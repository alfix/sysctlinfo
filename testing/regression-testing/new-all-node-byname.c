#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>

#include "sysctlinfo.h"

#define BUFSIZE    1024

int main()
{
	int *idp, idnext[CTL_MAXNAME], name2id[CTL_MAXNAME], op[2];
	size_t idlevel, idnextlevel, buflen, name2idlevel, i;
	char buf[BUFSIZE];
	unsigned int kind;
	char *name, *descr, *fmt, *label;
	char name_buf[1024];

	op[0] = CTL_SYSCTL;

	strcpy(name_buf, "sysctl");

	for (;;) {

		op[1] = SEROBJ_BYNAME;
		if (sysctl(op, 2, NULL, &buflen, name_buf, strlen(name_buf)+1) != 0) {
			printf("Error 'entry all len'\n");
			return (1);
		}
		memset(buf, 0, sizeof(buf));
		if (sysctl(op, 2, buf, &buflen, name_buf, strlen(name_buf)+1) != 0) {
			printf("Error 'all'\n");
			return (1);
		}
		
		SYSCTLINFO_DESOBJ(buf, idlevel, idp, name, descr, kind, fmt, label);
		
		printf("id [%zu]: ",idlevel);
		for (i = 0; i < idlevel; i++) {
			printf("%d%c", idp[i], i+1 < idlevel ? '.' : '\n');
		}
		
		/* namelen and name */
		printf("name [%zuB]: ", strlen(name)+1);
		printf("%s\n", name);

		/* nametoid */
		name2idlevel = CTL_MAXNAME * sizeof(int);
		memset(name2id, 0, sizeof(name2id));
		op[1] = OBJFAKEID_BYNAME;
		if (sysctl(op, 2, name2id, &name2idlevel, name, strlen(name) +1) != 0) {
			printf("Error 'name2id'\n");
			//return (1);
		}
		name2idlevel /= sizeof(int);
		printf("nametoid: ");
		for (i = 0; i < name2idlevel; i++) {
			printf("%d%c", name2id[i], i+1 < name2idlevel ? '.' : '\n');
		}

		/* description */
		if(descr == NULL)
			printf("descr [0B]: Error 'desc' or NULL\n\n");
		else
			printf("descr [%zuB]: %s\n", strlen(descr) +1, descr);

		/* label */
		if(label == NULL)
			printf("label [0B]: Error 'label' or NULL\n\n");
		else
			printf("label [%zuB]: %s\n", strlen(label) +1, label);

		/* kind */
		printf("kind: %u\n", kind );
		printf("\tflags: %u\n", kind & 0xfffffff0 );
		printf("\ttype: %u\n", kind & CTLTYPE );

		/* format string */
		//printf("fmt [%zuB]: ", buflen);
		printf("fmt: %s\n", fmt);
		
		printf("-------------------------------------------\n");

		/* nextleaf (or nextnode) */
		idnextlevel = CTL_MAXNAME * sizeof(int);
		op[1] = NEXTOBJNODE;
		if (sysctl(op, 2, idnext, &idnextlevel, idp, idlevel * sizeof(int)) != 0) {
			printf("Error next() or no next\n");
			break;
		}
		idnextlevel /= sizeof(int);
		
		/* nametoid */
		buflen = 1024;
		op[1] = OBJNAME;
		if (sysctl(op, 2, name_buf, &buflen, idnext, idnextlevel * sizeof(int)) != 0) {
			printf("Error 'update name to next name'\n");
			return (1);
		}

	} //end for(;;)

	return (0);
}
