#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>

#include "sysctlinfo.h"

#define BUFSIZE    1024

int main()
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME], name2id[CTL_MAXNAME], op[2];
	size_t idlevel, idnextlevel, buflen, i, name2idlevel;
	char buf[BUFSIZE];
	char name_buf[1024];

	op[0] = CTL_SYSCTL;

	strcpy(name_buf, "sysctl");

	for (;;) {
		idlevel = CTL_MAXNAME * sizeof(int);
		memset(id, 0, sizeof(id));
		op[1] = OBJID_BYNAME;
		if (sysctl(op, 2, id, &idlevel, name_buf, strlen(name_buf) + 1) != 0) {
			printf("Error 'name2id'\n");
			return (1);
		}
		idlevel /= sizeof(int);
		printf("id [%zu]: ", idlevel);
		for (i = 0; i < idlevel; i++) {
			printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
		}


		/* namelen and name */
		memset(buf, 0, sizeof(buf));
		printf("name [%zuB]: %s\n", strlen(name_buf) + 1, name_buf);
		
		/* nametoid */
		name2idlevel = CTL_MAXNAME * sizeof(int);
		memset(name2id, 0, sizeof(name2id));
		op[1] = OBJFAKEID_BYNAME;
		if (sysctl(op, 2, name2id, &name2idlevel, name_buf, strlen(name_buf) + 1) != 0) {
			printf("Error 'name2id'\n");
			//return (1);
		}
		name2idlevel /= sizeof(int);
		printf("nametoid: ");
		for (i = 0; i < name2idlevel; i++) {
			printf("%d%c", name2id[i], i+1 < name2idlevel ? '.' : '\n');
		}

		/* description */
		op[1] = OBJDESC_BYNAME;
		if (sysctl(op, 2, NULL, &buflen, name_buf, strlen(name_buf) + 1) != 0) {
			buflen = 0; /* NULL */
		}
		memset(buf, 0, sizeof(buf));
		printf("descr [%zuB]: ", buflen);
		if (sysctl(op, 2, buf, &buflen, name_buf, strlen(name_buf) + 1) != 0) {
			printf("Error 'desc' or NULL");
		}
		printf("%s\n", buf);

		/* label */
		op[1] = OBJLABEL_BYNAME;
		if (sysctl(op, 2, NULL, &buflen, name_buf, strlen(name_buf) + 1) != 0) {
			buflen = 0;
		}
		memset(buf, 0, sizeof(buf));
		printf("label [%zuB]: ", buflen);
		if (sysctl(op, 2, buf, &buflen, name_buf, strlen(name_buf) + 1) != 0) {
			printf("Error 'label' or NULL");
		}
		printf("%s\n", buf);

		/* kind */
		op[1] = OBJKIND_BYNAME;
		if (sysctl(op, 2, NULL, &buflen, name_buf, strlen(name_buf) + 1) != 0) {
		    if(buflen != sizeof(unsigned int))
			printf("kind bad size\n");
		}
		memset(buf, 0, sizeof(buf));
		if (sysctl(op, 2, buf, &buflen, name_buf, strlen(name_buf) + 1) != 0) {
			printf("Error 'kind'\n");
		}
		printf("kind: %u\n", *((u_int *)buf) );
		printf("\tflags: %u\n", *((u_int *)buf) & 0xfffffff0 );
		printf("\ttype: %u\n", *((u_int *)buf) & CTLTYPE );

		/* format string */
		op[1] = OBJFMT_BYNAME;
		if (sysctl(op, 2, NULL, &buflen, name_buf, strlen(name_buf) + 1) != 0) {
			buflen = 0;
		}
		memset(buf, 0, sizeof(buf));
		//printf("fmt [%zuB]: ", buflen);
		printf("fmt: ");
		if (sysctl(op, 2, buf, &buflen, name_buf, strlen(name_buf) + 1) != 0) {
			printf("Error 'fmt' or NULL\n");
		}
		printf("%s\n", buf);
		
		printf("-------------------------------------------\n");

		/* nextleaf (or nextnode) */
		idnextlevel = CTL_MAXNAME * sizeof(int);
		op[1] = FAKENEXTOBJLEAFNOSKIP; //NEXTOBJNODE;
		if (sysctl(op, 2, idnext, &idnextlevel, id, idlevel * sizeof(int)) != 0) {
			printf("Error next() or no next\n");
			break;
		}
		memcpy(id, idnext, idnextlevel);
		idlevel = idnextlevel / sizeof(int);
		
		op[1] = OBJNAME;
		buflen = 1024;
		sysctl(op, 2, name_buf, &buflen, id, idlevel * sizeof(int));
		//strcpy(name_buf, nextname);
	} //end for(;;)

	return (0);
}
