#include <sys/types.h>
#include <sys/queue.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>
#include <sysctlmibinfo.h>

#define BUFSIZE    1024

int main()
{
	int id[SYSCTLMIF_MAXIDLEVEL], idnext[SYSCTLMIF_MAXIDLEVEL];
	int name2id[CTL_MAXNAME];
	char buf[BUFSIZE];
	size_t idlevel, idnextlevel, buflen, i, name2idlevel;

	id[0] = 0;
	idlevel = 1;

	for (;;) {
		printf("id [%zu]: ",idlevel);
		for (i = 0; i < idlevel; i++) {
			printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
		}

		/* namelen and name */
		buflen = BUFSIZE;
		if (SYSCTLMIF_NAMELEN(id, idlevel, &buflen) != 0) {
			printf("Error 'namelen'\n");
			//return (1);
		}
		printf("name [%zuB]: ", buflen);
		if (sysctlmif_name(id, idlevel, buf, &buflen) != 0) {
			printf("Error 'name'\n");
			//return (1);
		}
		printf("%s\n", buf);
		
		/* nametoid */
		name2idlevel = CTL_MAXNAME * sizeof(int);
		memset(name2id, 0, sizeof(name2id));
		if (sysctlmif_nametoid(buf, buflen, name2id, &name2idlevel) != 0) {
			printf("Error 'name2id'\n");
			//return (1);
		}
		printf("nametoid: ");
		for (i = 0; i < name2idlevel; i++) {
			printf("%d%c", name2id[i], i+1 < name2idlevel ? '.' : '\n');
		}

		/* DESCLEN and desc */
		buflen = BUFSIZE;
		if (SYSCTLMIF_DESCLEN(id, idlevel, &buflen) != 0) {
			buflen = 0;
		}
		printf("descr [%zuB]: ", buflen);
		if (sysctlmif_desc(id, idlevel, buf, &buflen) != 0) {
			memset((void *)buf, 0, BUFSIZE);
			printf("Error 'desc' or NULL");
		}
		printf("%s\n", buf);

		/* LABELLEN and label */
		buflen = BUFSIZE;
		if (SYSCTLMIF_LABELLEN(id, idlevel, &buflen) != 0) {
			buflen = 0;
		}
		printf("label [%zuB]: ", buflen);
		if (sysctlmif_label(id, idlevel, buf, &buflen) != 0) {
			memset((void *)buf, 0, BUFSIZE);
			printf("Error 'label' or NULL");
		}
		printf("%s\n", buf);

		/* info: INFOKIND, INFOTYPE, INFOFLAGS, INFOFMT */
		buflen = BUFSIZE;
		if (sysctlmif_info(id, idlevel, (void *)buf, &buflen) != 0) {
			return (1);
		}
		printf("kind: %u\n", SYSCTLMIF_INFOKIND(buf));
		printf("\tflags: %u\n", SYSCTLMIF_INFOFLAGS(buf));
		printf("\ttype: %u\n", SYSCTLMIF_INFOTYPE(buf));
		printf("fmt: %s\n", SYSCTLMIF_INFOFMT(buf));

		printf("-------------------------------------------\n");

		/* nextleaf (or nextnode) */
		idnextlevel = SYSCTLMIF_MAXIDLEVEL;
		/* if(sysctlmif_nextnode(id,idlevel,idnext,&idnextlevel)!=0){ */
		if (sysctlmif_nextleaf(id, idlevel, idnext,
		    &idnextlevel) != 0) {
			printf("Error next() or no next\n");
			break;
		}
		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;
	} //end for(;;)

	return (0);
}
