#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>

#include "sysctlinfo.h"

#define BUFSIZE    1024

int main()
{
	int id[CTL_MAXNAME], *idp, *idnext, name2id[CTL_MAXNAME], op[2];
	size_t idlevel, idplevel, idnextlevel, buflen, name2idlevel, i;
	char buf[BUFSIZE];
	unsigned int kind;
	char *name, *descr, *fmt, *label;

	op[0] = CTL_SYSCTL;

	id[0]=0;
	idlevel=1;

	for (;;) {
		op[1] = SEROBJNEXTLEAF;
		if (sysctl(op, 2, NULL, &buflen, id, idlevel * sizeof(int)) != 0) {
			printf("Error 'all len'\n");
			return (1);
		}
		memset(buf, 0, sizeof(buf));
		//printf("all [%zuB]: ", buflen);
		if (sysctl(op, 2, buf, &buflen, id, idlevel * sizeof(int)) != 0) {
			printf("Error 'all'\n");
			return (1);
		}
		
		SYSCTLINFO_DESOBJ_NEXTOID(&buf, idplevel, idp, name, descr, kind, fmt, label, idnextlevel, idnext);
		
		if(kind & CTLFLAG_SKIP) {
			memcpy(id, idnext, idnextlevel * sizeof(int));
			idlevel = idnextlevel;
			continue;
		}

		printf("id [%zu]: ",idlevel);
		for (i = 0; i < idlevel; i++) {
			printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
		}

		//check idp and idplevel
		if(idlevel != idplevel) {
			printf("%s idlevel != idplevel\n", name);
			return 1;
		}
		for (i = 0; i < idlevel; i++) {
			if(id[i] != idp[i]) {
				printf("%s id[%zu] != idp[%zu]\n", name, i, i);
				return 1;
			}
		}
		
		/* namelen and name */
		printf("name [%zuB]: ", strlen(name)+1);
		printf("%s\n", name);

		/* nametoid */
		name2idlevel = CTL_MAXNAME * sizeof(int);
		memset(name2id, 0, sizeof(name2id));
		op[1] = OBJFAKEID_BYNAME;
		if (sysctl(op, 2, name2id, &name2idlevel, name, strlen(name) +1) != 0) {
			printf("Error 'name2id'\n");
			//return (1);
		}
		name2idlevel /= sizeof(int);
		printf("nametoid: ");
		for (i = 0; i < name2idlevel; i++) {
			printf("%d%c", name2id[i], i+1 < name2idlevel ? '.' : '\n');
		}

		/* description */
		if(descr == NULL)
			printf("descr [0B]: Error 'desc' or NULL\n\n");
		else
			printf("descr [%zuB]: %s\n", strlen(descr) +1, descr);

		/* label */
		if(label == NULL)
			printf("label [0B]: Error 'label' or NULL\n\n");
		else
			printf("label [%zuB]: %s\n", strlen(label) +1, label);

		/* kind */
		printf("kind: %u\n", kind );
		printf("\tflags: %u\n", kind & 0xfffffff0 );
		printf("\ttype: %u\n", kind & CTLTYPE );

		/* format string */
		//printf("fmt [%zuB]: ", buflen);
		printf("fmt: %s\n", fmt);
		
		printf("-------------------------------------------\n");

		/* nextleaf (or nextnode) */
		if (idnextlevel <= 0) {
			printf("Error next() or no next\n");
			break;
		}
		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;
	} //end for(;;)

	return (0);
}
