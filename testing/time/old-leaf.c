#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>

#define BUFSIZE    1024

int main()
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME];
	int mib[CTL_MAXNAME], name2id[CTL_MAXNAME];
	char buf[BUFSIZE];
	size_t idlevel, idnextlevel, buflen, i, name2idlevel, miblen;

	mib[0] = 0;
	id[0] = 0;
	idlevel = 1;

	for (;;) {
		memcpy(mib + 2, id, idlevel * sizeof(int));
		miblen = idlevel + 2;
		
		printf("id [%zu]: ",idlevel);
		for (i = 0; i < idlevel; i++) {
			printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
		}

		// name
		buflen = BUFSIZE;
		mib[1] = 1;
		if(sysctl(mib, miblen, NULL, &buflen, NULL, 0) != 0) {
			printf("Error 'namelen'\n");
			return (1);
		}
		printf("name [%zuB]: ", buflen);
		if(sysctl(mib, miblen, buf, &buflen, NULL, 0) != 0) {
			printf("Error 'name'\n");
			return (1);
		}
		printf("%s\n", buf);
		
		// nametoid
		name2idlevel = CTL_MAXNAME * sizeof(int);
		//memset(name2id, 0, sizeof(name2id));
		mib[1] = 3;
		if (sysctl(mib, 2, name2id, &name2idlevel, buf, buflen) != 0) {
			printf("Error 'name2id'\n");
			return (1);
		}
		name2idlevel /= sizeof(int);
		printf("nametoid: ");
		for (i = 0; i < name2idlevel; i++) {
			printf("%d%c", name2id[i], i+1 < name2idlevel ? '.' : '\n');
		}

		// description
		buflen = BUFSIZE;
		memcpy(mib + 2, id, idlevel * sizeof(int));
		miblen = idlevel + 2;
		mib[1] = 5;
		if(sysctl(mib, miblen, NULL, &buflen, NULL, 0) != 0) {
			buflen = 0;
		}
		printf("descr [%zuB]: ", buflen);
		if(sysctl(mib, miblen, buf, &buflen, NULL, 0) != 0) {
			memset((void *)buf, 0, BUFSIZE);
			printf("Error 'desc' or NULL");
		}
		printf("%s\n", buf);

		// label 
		buflen = BUFSIZE;
		mib[1] = 6;
		if(sysctl(mib, miblen, NULL, &buflen, NULL, 0) != 0) {
			buflen = 0;
		}
		printf("label [%zuB]: ", buflen);
		if(sysctl(mib, miblen, buf, &buflen, NULL, 0) != 0) {
			memset((void *)buf, 0, BUFSIZE);
			printf("Error 'label' or NULL");
		}
		printf("%s\n", buf);

		// info: kind, flags, type, fmt
		buflen = BUFSIZE;
		mib[1] = 4;
		if(sysctl(mib, miblen, NULL, &buflen, NULL, 0) != 0) {
			printf("Error 'kindlen'\n");
			return (1);
		}
		if(sysctl(mib, miblen, buf, &buflen, NULL, 0) != 0) {
			printf("Error 'kind'\n");
			return (1);
		}
		printf("kind: %u\n", *((unsigned int *)buf) );
		printf("\tflags: %u\n", *((unsigned int *)buf) & 0xfffffff0 );
		printf("\ttype: %u\n", *((unsigned int *)buf) & CTLTYPE );
		printf("fmt: %s\n", (char *)buf + sizeof(unsigned int) );

		printf("-------------------------------------------\n");

		// nextleaf
		idnextlevel = CTL_MAXNAME * sizeof(int);
		mib[1] = 2;
		if(sysctl(mib, miblen, idnext, &idnextlevel, NULL , 0) != 0) {
			printf("Error next() or no next\n");
			break;
		}
		memcpy(id, idnext, idnextlevel);
		idlevel = idnextlevel / sizeof(int);
	} //end for(;;)

	return (0);
}
