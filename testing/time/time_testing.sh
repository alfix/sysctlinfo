#! /bin/sh

d=${1:-`pwd`};shift

progs="old-leaf new-leaf new-all-leaf new-all-leaf-byname"

for p in $progs
do
	cc -I../../ ${p}.c -o ${p}
done

counter=0

while [ $counter -lt 30 ]
do
	for p in $progs
	do
		time -a -o ${d}/time_${p}.txt ${d}/${p}
	done
	counter=`expr $counter + 1`
done

rm -f total_time.txt

for p in $progs
do
	echo ${p} >> total_time.txt
	awk '{ s += $1 } END { print "real: ", s/NR}' time_${p}.txt >> total_time.txt
	awk '{ s += $3 } END { print "user: ", s/NR}' time_${p}.txt >> total_time.txt
	awk '{ s += $5 } END { print "sys:  ", s/NR}' time_${p}.txt >> total_time.txt
done

cat total_time.txt
rm -f total_time.txt

for p in $progs
do
	rm -f ${p}
	rm -f time_${p}.txt
done
