#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>

int main()
{
	/* nsysctl -Ny security.jail.param.allow.mount. to get its ID */
	int oid[6] = {2147482889, 2147482976, 2147482963, 2147482931, 2147482920, 2147482919};
	int value;
	size_t sizeint;

	sizeint = sizeof(int);
	if(sysctlbyname("security.jail.param.allow.mount.", &value, &sizeint, NULL,0) != 0)
		printf("error byname\n");
	else
		printf("byname: %d\n", value);

	sizeint = sizeof(int);
	if(sysctl(oid, 6, &value, &sizeint, NULL,0) != 0)
		printf("error sysctl\n");
	else
		printf("sysctl: %d\n", value);


	return 0;
}

