#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>

#include "../sysctlinfo.h"

void print_name(int *id, size_t idlevel);

#define BUFSIZE    1024

int main()
{
    int id[CTL_MAXNAME], i;
    size_t idlevel;

    printf("\n=== GOOD name ===\n\n");
	
    id[0]=1;
    id[1]=1;
    idlevel=2;
    
    print_name(id, idlevel);
    
    
    printf("\n=== Bad name ===\n\n");
    
    id[0]=1;
    id[1]=1;
    id[2]=100;
    id[3]=500;
    id[4]=1000;
    idlevel=5;
    
    print_name(id, idlevel);
    
    printf("\n=== BAD 2 name ===\n\n");

    id[0]=2147482899;
    id[1]=2147481612;
    id[2]=2147481611;
    id[3]=2147482899;
    id[4]=2147481612;
    id[5]=2147481611;
    idlevel=6;
    
    print_name(id, idlevel);

    printf("\n 24 LEVELS \n");
    
    printf("\n=== GOOD name ===\n\n");

    char *dchar = "c1.c2.c3.c4.c5.c6.c7.c8.c9.c10.c11.c12.c13.c14.c15.c16.c17.c18.c19.c20.c21.c22.c23.c24";
    bzero(id, sizeof(id));
    idlevel = CTL_MAXNAME;
    if (sysctlnametomib(dchar, id, &idlevel) != 0)
	    printf("error sysctlnametomib\n");
    else
    	print_name(id, idlevel);
    
    printf("\n=== Bad name ===\n\n");
    
    id[0]=100;
    idlevel=24;
    
    print_name(id, idlevel);

    return (0);
}

void
print_name(int *id, size_t idlevel)
{
int i;
    char buf[BUFSIZE];
    size_t buflen;
    int mib[CTL_MAXNAME + 2];
    
    mib[0] = CTL_SYSCTL;

    printf("id [%zu]: ",idlevel);
    for (i = 0; i < idlevel; i++) {
	printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
    }
    
    mib[1] = 1; /* Old API*/
    printf("sysctl.name\n");
    buflen = BUFSIZE;
    memcpy(mib + 2, id, idlevel * sizeof(int));

    if (sysctl(mib, idlevel + 2, NULL, &buflen, NULL, 0) != 0)
	printf("\tError NAMELEN(), ");

    memset(buf, 0, sizeof(buf));
    printf("\tname [%zuB]: ", buflen);
    if (sysctl(mib, idlevel + 2, buf, &buflen, NULL, 0) != 0)
	printf("Error _name()\n");
    else
    	printf("\"%s\"\n", buf);

	mib[1] = ENTRYNAME;
    printf("sysctl.entryname\n");

    buflen = BUFSIZE;
    if (sysctl(mib, 2, NULL ,&buflen, id, idlevel * sizeof(int)) != 0)
	printf("\tError NAMELEN(), ");

    memset(buf, 0, sizeof(buf));
    printf("\tname [%zuB]: ", buflen);
    if (sysctl(mib, 2, buf ,&buflen, id, idlevel * sizeof(int)) != 0)
	printf("Error _name()\n");
    else
    	printf("\"%s\"\n", buf);
    
    mib[1] = ENTRYFAKENAME;
    printf("sysctl.entryfakename\n");

    buflen = BUFSIZE;
    if (sysctl(mib, 2, NULL ,&buflen, id, idlevel * sizeof(int)) != 0)
	printf("\tError NAMELEN(), ");

    memset(buf, 0, sizeof(buf));
    printf("\tname [%zuB]: ", buflen);
    if (sysctl(mib, 2, buf ,&buflen, id, idlevel * sizeof(int)) != 0)
	printf("Error _name()\n");
    else
    	printf("\"%s\"\n", buf);
}
