#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/sysctl.h>

static struct sysctl_ctx_list clist;
static struct sysctl_oid *x, *y, *z;
static int xlevel, ylevel, zlevel;

/*
 * name == desc
 * level == value
 */
static struct sysctl_oid *
addnode(const char* strname, int *level)
{
    int i;
    static struct sysctl_oid *obj, *tmpnode;
    char buf[4];

    snprintf(buf, sizeof(buf), "%s%d", strname, 1);
    tmpnode = obj = SYSCTL_ADD_ROOT_NODE(NULL, OID_AUTO, buf, CTLFLAG_RW, 0, buf);
    uprintf("%s", buf);
    for(i=2; i < *level; i++) {
	snprintf(buf, sizeof(buf), "%s%d", strname, i);
	tmpnode = SYSCTL_ADD_NODE(NULL, SYSCTL_CHILDREN(tmpnode), OID_AUTO, buf,CTLFLAG_RW, 0, buf);
	uprintf(".%s", buf);
    }
    snprintf(buf, sizeof(buf), "%s%d", strname, i);
    SYSCTL_ADD_INT(NULL, SYSCTL_CHILDREN(tmpnode), OID_AUTO, buf, CTLFLAG_RW, level, 0, buf);
    uprintf(".%s\n", buf);

    return obj;
}

static int
pointless_modevent(module_t mod __unused, int event, void *arg __unused)
{
    int error = 0;
    
    switch (event) {
    case MOD_LOAD:
	sysctl_ctx_init(&clist);
	zlevel = CTL_MAXNAME - 2;
	ylevel = CTL_MAXNAME - 1;
	xlevel = CTL_MAXNAME;
	z = addnode("z", &zlevel);
	y = addnode("y", &ylevel);
	x = addnode("x", &xlevel);
	uprintf("Pointless module loaded.\n");
	break;
    case MOD_UNLOAD:
	sysctl_remove_oid(z,1,1);
	sysctl_remove_oid(y,1,1);
	sysctl_remove_oid(x,1,1);
	if (sysctl_ctx_free(&clist)) {
	    uprintf("sysctl_ctx_free failed.\n");
	    return (ENOTEMPTY);
	}
	uprintf("Pointless module unloaded.\n");
	break;
    default:
	error = EOPNOTSUPP;
	break;
    }
    return (error);
}
static moduledata_t pointless_mod = {
    "pointless",
    pointless_modevent,
    NULL
};

DECLARE_MODULE(pointless, pointless_mod, SI_SUB_EXEC, SI_ORDER_ANY);
