#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>

#include "../sysctlinfo.h"

#define BUFSIZE    1024

int main()
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME], name2id[CTL_MAXNAME], op[2];
	size_t idlevel, idnextlevel, buflen, name2idlevel, i;
	char buf[BUFSIZE];

	op[0] = CTL_SYSCTL;

	id[0]=1;
	id[1]=1;
	idlevel=2;

	for (;;) {
		if(idlevel < 22)
			goto next;

		printf("id [%zu]: ",idlevel);
		for (i = 0; i < idlevel; i++) {
			printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
		}

		/* name */
		op[1] = ENTRYNAME;
		if (sysctl(op, 2, NULL, &buflen, id, idlevel * sizeof(int)) != 0) {
			printf("Error 'namelen'\n");
			//return (1);
		}
		memset(buf, 0, sizeof(buf));
		printf("name [%zuB]: ", buflen);
		if (sysctl(op, 2, buf, &buflen, id, idlevel * sizeof(int)) != 0) {
			printf("Error 'name'\n");
			//return (1);
		}
		printf("%s\n", buf);

		/* nametoid */
		name2idlevel = CTL_MAXNAME * sizeof(int);
		memset(name2id, 0, sizeof(name2id));
		op[1] = ENTRYNAME2ID;
		if (sysctl(op, 2, name2id, &name2idlevel, buf, buflen) != 0) {
			printf("Error 'name2id'\n");
			//return (1);
		}
		name2idlevel /= sizeof(int);
		printf("nametoid: ");
		for (i = 0; i < name2idlevel; i++) {
			printf("%d%c", name2id[i], i+1 < name2idlevel ? '.' : '\n');
		}

		/* description */
		op[1] = ENTRYDESC;
		if (sysctl(op, 2, NULL, &buflen, id, idlevel * sizeof(int)) != 0) {
			buflen = 0; /* NULL */
		}
		memset(buf, 0, sizeof(buf));
		printf("descr [%zuB]: ", buflen);
		if (sysctl(op, 2, buf, &buflen, id, idlevel * sizeof(int)) != 0) {
			printf("Error 'desc' or NULL\n");
		}
		printf("%s\n", buf);

		/* label */
		op[1] = ENTRYLABEL;
		if (sysctl(op, 2, NULL, &buflen, id, idlevel * sizeof(int)) != 0) {
			buflen = 0;
		}
		memset(buf, 0, sizeof(buf));
		printf("label [%zuB]: ", buflen);
		if (sysctl(op, 2, buf, &buflen, id, idlevel * sizeof(int)) != 0) {
			printf("Error 'label' or NULL");
		}
		printf("%s\n", buf);

		/* kind */
		op[1] = ENTRYKIND;
		if (sysctl(op, 2, NULL, &buflen, id, idlevel * sizeof(int)) != 0) {
		    if(buflen != sizeof(unsigned int))
			printf("kind bad size\n");
		}
		memset(buf, 0, sizeof(buf));
		if (sysctl(op, 2, buf, &buflen, id, idlevel * sizeof(int)) != 0) {
			printf("Error 'kind'\n");
		}
		printf("kind: %u\n", *((u_int *)buf) );
		printf("\tflags: %u\n", *((u_int *)buf) & 0xfffffff0 );
		printf("\ttype: %u\n", *((u_int *)buf) & CTLTYPE );

		/* format string */
		op[1] = ENTRYFMT;
		if (sysctl(op, 2, NULL, &buflen, id, idlevel * sizeof(int)) != 0) {
			buflen = 0;
		}
		memset(buf, 0, sizeof(buf));
		printf("fmt [%zuB]: ", buflen);
		if (sysctl(op, 2, buf, &buflen, id, idlevel * sizeof(int)) != 0) {
			printf("Error 'fmt' or NULL\n");
		}
		printf("%s\n", buf);
		
		printf("-------------------------------------------\n");

next:
		/* nextleaf (or nextnode) */
		idnextlevel = CTL_MAXNAME * sizeof(int);
		op[1] = ENTRYNEXTLEAF;
		if (sysctl(op, 2, idnext, &idnextlevel, id, idlevel * sizeof(int)) != 0) {
			printf("Error next() or no next\n");
			break;
		}
		memcpy(id, idnext, idnextlevel);
		idlevel = idnextlevel / sizeof(int);
	} //end for(;;)

	return (0);
}
