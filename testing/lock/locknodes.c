#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/sysctl.h>
#include <sys/proc.h>

static struct sysctl_ctx_list clist;
static struct sysctl_oid *before_handler, *in_handler;
static int intvalue;

static int
inh2_handler(SYSCTL_HANDLER_ARGS)
{
	int i;
	
	for (i = 0; i < 10; i++) {
		uprintf("inh2_handler %d\n", i);
		pause("inh sleep", 1000);
	}
	return 0;
}

static void addnode()
{
    int i, level = 5;
    static struct sysctl_oid *obj;
    char buf[4];

    intvalue = level;

    // bh1. ... .bh5
    snprintf(buf, sizeof(buf), "%s%d", "bh", 1);
    before_handler = obj = SYSCTL_ADD_ROOT_NODE(NULL, OID_AUTO, buf, CTLFLAG_RW, 0, buf);
    uprintf("%s", buf);
    for(i=2; i < level; i++) {
	snprintf(buf, sizeof(buf), "%s%d", "bh", i);
	obj = SYSCTL_ADD_NODE(NULL, SYSCTL_CHILDREN(obj), OID_AUTO, buf,CTLFLAG_RW, 0, buf);
	uprintf(".%s", buf);
    }
    snprintf(buf, sizeof(buf), "%s%d", "bh", i);
    SYSCTL_ADD_INT(NULL, SYSCTL_CHILDREN(obj), OID_AUTO, buf, CTLFLAG_RW, &intvalue, 0, "test oid");
    uprintf(".%s\n", buf);
    
    // inh1.inh2
    in_handler = obj = SYSCTL_ADD_ROOT_NODE(NULL, OID_AUTO, "inh1", CTLFLAG_RW, 0, "inh1");

    SYSCTL_ADD_PROC(NULL, SYSCTL_CHILDREN(obj), OID_AUTO, "inh2", CTLTYPE_STRING | CTLFLAG_RD | CTLFLAG_MPSAFE, 
        NULL, 0, inh2_handler, "A", "inh2 test oid");
    uprintf("inh1.inh2\n");
}

static int
pointless_modevent(module_t mod __unused, int event, void *arg __unused)
{
    int error = 0;
    
    switch (event) {
    case MOD_LOAD:
	sysctl_ctx_init(&clist);
	addnode();
	uprintf("test lock module loaded.\n");
	break;
    case MOD_UNLOAD:
	if(sysctl_remove_oid(before_handler,1,1) != 0)
		uprintf("no remove bh tree\n");
	if(sysctl_remove_oid(in_handler,1,1) !=0)
		uprintf("no remove inh tree\n");
	if (sysctl_ctx_free(&clist)) {
	    uprintf("sysctl_ctx_free failed.\n");
	    return (ENOTEMPTY);
	}
	uprintf("test lock module unloaded.\n");
	break;
    default:
	error = EOPNOTSUPP;
	break;
    }
    return (error);
}
static moduledata_t pointless_mod = {
    "pointless",
    pointless_modevent,
    NULL
};

DECLARE_MODULE(pointless, pointless_mod, SI_SUB_EXEC, SI_ORDER_ANY);
