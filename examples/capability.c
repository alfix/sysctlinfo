/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written by Alfonso Sabato Siciliano <https://alfonsosiciliano.gitlab.io>.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>
#include <sys/capsicum.h>
#include <sys/sysctl.h>

#include <errno.h>
#include <libutil.h>
#include <stdio.h>
#include <string.h>
#include <sysctlinfo.h>

#define BUFSIZE    1024

/* Print object name in capability mode */
int main()
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME], prop[2];
	size_t idlevel, idnextlevel, buflen;
	char buf[BUFSIZE];

	if(kld_isloaded("sysctlinfo") == 0) {
		printf("sysctlinfo.ko is not loaded");
		return (1);
	}

	if(cap_enter() < 0 && errno != ENOSYS) {
		printf("Unable to enter capability mode");
		return (1);
	}
	
	id[0] = 1;
	idlevel = 1;
	prop[0] = CTL_SYSCTL;
	for (;;) {
		/* get object name */
		prop[1] = OBJNAME;
		buflen = BUFSIZE;
		if (SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0) {
			if (errno == ECAPMODE)
				goto next;
			return (1);
		}
		printf("%s:", buf);
		
		/* kind for flags */
		prop[1] = OBJKIND;
		buflen = sizeof(unsigned int);
		if (SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0) {
			if (errno == ECAPMODE) /* useless: tested above  */
				goto next;
			return (1);
		}
		if(*((unsigned int*)buf) & CTLFLAG_CAPRD)
			printf(" CAPRD");
		if(*((unsigned int*)buf) & CTLFLAG_CAPWR)
			printf(" CAPWR");
		printf("\n");

next:
		/* next */
		idnextlevel = CTL_MAXNAME * sizeof(int);
		prop[1] = NEXTOBJNODE;
		if (SYSCTLINFO(id, idlevel, prop, idnext, &idnextlevel) != 0) {
			if(errno == ENOATTR)
				break;
			return (1);
		}
		memcpy(id, idnext, idnextlevel);
		idlevel = idnextlevel / sizeof(int);
	}
	
	return 0;
}
