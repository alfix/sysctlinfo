/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written by Alfonso Sabato Siciliano <https://alfonsosiciliano.gitlab.io>.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <errno.h>
#include <libutil.h>
#include <stdio.h>
#include <string.h>
#include <sysctlinfo.h>

#define BUFSIZE    1024

/* 
 * Visit (Depth-first search) the sysctl MIB-Tree to print the info of the 
 * nodes, you can choose to show all nodes or only leaves.
 */
int main()
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME], name2id[CTL_MAXNAME], prop[2];
	size_t idlevel, idnextlevel, buflen, i, name2idlevel;
	char buf[BUFSIZE];

	if(kld_isloaded("sysctlinfo") == 0) {
		printf("sysctlinfo.ko is not loaded");
		return (1);
	}

	id[0] = 0;
	idlevel = 1;
	prop[0] = CTL_SYSCTL;
	for (;;) {
		printf("OID [%zu]: ",idlevel);
		for (i = 0; i < idlevel; i++)
			printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');

		/* name */
		prop[1] = OBJNAME;
		if (SYSCTLINFO(id, idlevel, prop, NULL, &buflen) != 0)
			return (1);
		printf("name [%zuB]: ", buflen);
		/* possible: buf = malloc(buflen) */
		if (SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0)
			return (1);
		printf("%s\n", buf);
		
		/* OID by name, quite useless here, just for example */
		name2idlevel = CTL_MAXNAME * sizeof(int);
		memset(name2id, 0, sizeof(name2id));
		prop[1] = OBJID_BYNAME; /* or OBJFAKEID_BYNAME */
		if (SYSCTLINFO_BYNAME(buf, prop, name2id, &name2idlevel) != 0)
			return (1);
		name2idlevel /= sizeof(int);
		printf("OID by name: ");
		for (i = 0; i < name2idlevel; i++)
			printf("%d%c", name2id[i], i+1 < name2idlevel ? '.' : '\n');

		/* description */
		prop[1] = OBJDESC;
		if (SYSCTLINFO(id, idlevel, prop, NULL, &buflen) != 0) {
			if(errno != ENOATTR)
				return (1);
			buflen = 0;
		}
		printf("descr [%zuB]: ", buflen);
		if (SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0)
			buf[0]='\0';
		printf("%s\n", buf);

		/* label */
		prop[1] = OBJLABEL;
		if (SYSCTLINFO(id, idlevel, prop, NULL, &buflen) != 0) {
			if(errno != ENOATTR)
				return (1);
			buflen = 0;
		}
		printf("label [%zuB]: ", buflen);
		if (SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0)
			buf[0]='\0';
		printf("%s\n", buf);

		/* kind */
		prop[1] = OBJKIND;
		buflen = sizeof(unsigned int);
		if (SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0)
			return (1);

		printf("kind: %u\n",    *((u_int *)buf));
		printf("\tflags: %u\n", *((u_int *)buf) & 0xfffffff0);
		printf("\ttype:  %u\n", *((u_int *)buf) & CTLTYPE);

		/* format string */
		prop[1] = OBJFMT;
		if (SYSCTLINFO(id, idlevel, prop, NULL, &buflen) != 0)
			return (1);
		printf("fmt [%zuB]: ", buflen);
		if (SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0)
			return(1);
		printf("%s\n", buf);
		
		printf("-------------------------------------------\n");

		/* next */
		prop[1] = NEXTOBJNODE; /* or NEXTOBJLEAF */
		idnextlevel = CTL_MAXNAME * sizeof(int);
		if (SYSCTLINFO(id, idlevel, prop, idnext, &idnextlevel) != 0) {
			if(errno == ENOATTR)
				break;
			return (1);
		}
		memcpy(id, idnext, idnextlevel);
		idlevel = idnextlevel / sizeof(int);
	}

	return (0);
}
