/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written by Alfonso Sabato Siciliano <https://alfonsosiciliano.gitlab.io>.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <libutil.h>
#include <stdio.h>
#include <string.h>
#include <sysctlinfo.h>

#define BUFSIZE    1024

/* 
 * Examples: SEROBJNEXTNODE_BYNAME or SEROBYNEXTLEAF_BYNAME.
 *
 * Visit (Depth-first search) the sysctl MIB-Tree to print the info of the 
 * nodes, you can choose to show all nodes or only leaves.
 */
int main()
{
	int *id, prop[2];
	size_t idlevel, buflen, i;
	char buf[BUFSIZE], name_buf[1024], *name, *descr, *fmt, *label, *nextname;
	unsigned int kind;
	
	prop[0] = CTL_SYSCTL;
	prop[1] = SEROBJNEXTLEAF_BYNAME; /* or SEROBJNEXTNODE_BYNAME */
	
	if(kld_isloaded("sysctlinfo") == 0) {
		printf("sysctlinfo.ko is not loaded");
		return (1);
	}

	strcpy(name_buf, "sysctl");
	for (;;) {
		if (SYSCTLINFO_BYNAME(name_buf, prop, NULL, &buflen) != 0)
			return (1);
		/* You could use: buf = malloc(buflen) */
		if (SYSCTLINFO_BYNAME(name_buf, prop, buf, &buflen) != 0)
			return (1);
		
		SYSCTLINFO_DESOBJ_NEXTNAME(buf, idlevel, id, name, descr, kind,
		    fmt, label, nextname);
		
		printf("id [%zu]: ",idlevel);
		for (i = 0; i < idlevel; i++)
			printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
		
		printf("name:  %s\n",   name);
		printf("desc:  %s\n",   descr);
		printf("label: %s\n",   label);
		printf("kind:  %u\n",   kind);
		printf("\tflags: %u\n", kind & 0xfffffff0);
		printf("\ttype:  %u\n", kind & CTLTYPE);
		printf("fmt:   %s\n",   fmt);	
		printf("-------------------------------------------\n");

		if (nextname[0] == '\0')
			break;
		strcpy(name_buf, nextname);
	}
	
	return (0);
}
