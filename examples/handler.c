/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written by Alfonso Sabato Siciliano <https://alfonsosiciliano.gitlab.io>.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <libutil.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sysctlinfo.h>

int main()
{
	int id[3] = {CTL_KERN, KERN_PROC, KERN_PROC_PID}, prop[2];
	size_t boolsize = sizeof(bool);
	bool handler;
	
	prop[0] = CTL_SYSCTL;
	
	if(kld_isloaded("sysctlinfo") == 0) {
		printf("sysctlinfo.ko is not loaded");
		return (1);
	}

	/* OID */
	prop[1] = OBJHASHANDLER;
	if(SYSCTLINFO(id, 3, prop, &handler, &boolsize) != 0)
		return (1);
	printf("Has `kern.proc.pid` handler? %s\n", handler ? "true" : "false");

	if(SYSCTLINFO(id, 2, prop, &handler, &boolsize) != 0)
		return (1);
	printf("Has `kern.proc` handler? %s\n", handler ? "true" : "false");

	/* Name */
	prop[1] = OBJHASHANDLER_BYNAME;
	if(SYSCTLINFO_BYNAME("kern.proc.pid", prop, &handler, &boolsize) != 0)
		return (1);
	printf("Has `kern.proc.pid` handler? %s\n", handler ? "true" : "false");

	if(SYSCTLINFO_BYNAME("kern.proc", prop, &handler, &boolsize) != 0)
		return (1);
	printf("Has `kern.proc` handler? %s\n", handler ? "true" : "false");

	return (0);
}
