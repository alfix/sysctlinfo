/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written by Alfonso Sabato Siciliano <https://alfonsosiciliano.gitlab.io>.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <libutil.h>
#include <stdio.h>
#include <string.h>
#include <sysctlinfo.h>

#define BUFSIZE    1024

/* 
 * Examples: SEROBJNEXTLEAF or SEROBJNEXTNODE.
 *
 * Visit (Depth-first search) the sysctl MIB-Tree to print the info of the 
 * nodes, you can choose to show all nodes or only leaves.
 */
int main()
{
	int id[CTL_MAXNAME], *idp_unused, *idnext, prop[2];
	size_t idlevel, idnextlevel, buflen, i;
	char buf[BUFSIZE], *namep, *descrp, *fmtp, *labelp;
	unsigned int kind;

	if(kld_isloaded("sysctlinfo") == 0) {
		printf("sysctlinfo.ko is not loaded");
		return (1);
	}

	prop[0] = CTL_SYSCTL;
	prop[1] = SEROBJNEXTLEAF; /* or SEROBJNEXTNODE; */

	id[0]=0;
	idlevel=1;
	for (;;) {
		if(SYSCTLINFO(id, idlevel, prop, NULL, &buflen) != 0)
			return (1);
		/* You could use: buf = malloc(buflen) */
		if(SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0)
			return (1);
	
		SYSCTLINFO_DESOBJ_NEXTOID(buf, idlevel, idp_unused, namep,
		    descrp, kind, fmtp, labelp, idnextlevel, idnext);

		printf("OID [%zu]: ", idlevel);
		for (i = 0; i < idlevel; i++)
			printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');

		printf("name:  %s\n",   namep);
		printf("desc:  %s\n",   descrp);
		printf("label: %s\n",   labelp);
		printf("kind:  %u\n",   kind);
		printf("\tflags: %u\n", kind & 0xfffffff0);
		printf("\ttype:  %u\n", kind & CTLTYPE);
		printf("fmt:   %s\n",   fmtp);
		printf("-------------------------------------------\n");

		if (idnextlevel < 1)
			break;			
		memcpy(id, idnext, idnextlevel * sizeof(int));
		idlevel = idnextlevel;
	}

	return (0);
}
