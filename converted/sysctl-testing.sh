#!/bin/sh

d=${1:-`pwd`};shift
prog=sysctl
conv=${d}/sysctl-sysctlinfo

rm -f ${conv}
cc ${conv}.c -o ${conv} -lutil

# INFO (Important)
for t in -aN -aNT -aNW -ad -adT -adW -at -atd
do
    echo "$t"
    ${prog} $t >> ${prog}.txt
    ${conv} $t > ${conv}.txt
    diff ${prog}.txt ${conv}.txt | diffstat -m
    rm ${prog}.txt ${conv}.txt
done

# VALUE (Optional)
for t in -aT -aTo -aWo -aWx -aW -ao -a
do
    echo "$t"
    ${prog} $t >> ${prog}.txt
    ${conv} ${t}  >> ${conv}.txt
    diff ${prog}.txt ${conv}.txt | diffstat -m
    rm ${prog}.txt ${conv}.txt
done

rm ${conv}
