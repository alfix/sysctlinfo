/* 
	% cc -I../ sysctlbyname_compare_example.c
	% ./a.out
*/

/*-
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright 2001 The FreeBSD Project. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE FREEBSD PROJECT ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE FREEBSD PROJECT BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
 
 /*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@FreeBSD.org> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 *
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <string.h>
#include <sysctlinfo.h>

#include <stdio.h>

int
sysctlnametomib_sysctlinfo(const char *name, int *mibp, size_t *sizep)
{
	int prop[2] = {CTL_SYSCTL, OBJID_BYNAME};
	int error;

	*sizep *= sizeof(int);
	error = SYSCTLINFO_BYNAME(name, prop, mibp, sizep);
	*sizep /= sizeof(int);
	return (error);
}

int
sysctlbyname_sysctlinfo(const char *name, void *oldp, size_t *oldlenp,
    const void *newp, size_t newlen)
{
	int real_oid[CTL_MAXNAME];
	size_t oidlen = CTL_MAXNAME;

	if (sysctlnametomib_sysctlinfo(name, real_oid, &oidlen) < 0)
		return (-1);
	return (sysctl(real_oid, oidlen, oldp, oldlenp, newp, newlen));
}

int main()
{
	int error, value;
	size_t valuelen = sizeof(int);

	error = sysctlbyname_sysctlinfo("security.jail.param.allow.mount.", &value, &valuelen, NULL, 0);
	printf("sysctlbyname_sysctlinfo, error: %d, value: %d, valuelen: %lu\n", error, value, valuelen);

	valuelen = sizeof(int);
	error = sysctlbyname("security.jail.param.allow.mount.", &value, &valuelen, NULL, 0);
	printf("sysctlbyname, error: %d, value: %d, valuelen: %lu\n", error, value, valuelen);

	return 0;
}
