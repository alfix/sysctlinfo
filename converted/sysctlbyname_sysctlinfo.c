/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@FreeBSD.org> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 *
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD: head/lib/libc/gen/sysctlbyname.c 244153 2012-12-12 15:27:33Z pjd $");

#include <sys/types.h>
#include <sys/sysctl.h>

int
sysctlbyname_sysctlinfo(const char *name, void *oldp, size_t *oldlenp,
    const void *newp, size_t newlen)
{
	int real_oid[CTL_MAXNAME];
	size_t oidlen = CTL_MAXNAME;

	if (sysctlnametomib_sysctlinfo(name, real_oid, &oidlen) < 0)
		return (-1);
	return (sysctl(real_oid, oidlen, oldp, oldlenp, newp, newlen));
}
