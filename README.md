sysctlinfo
==========

Index

 1.  [Introduction](#1-introduction)
 2.  [Description](#2-description)
 3.  [Getting started](#3-getting-started)
 4.  [Features](#4-features)
 5.  [API](#5-api)
 6.  [Manuals](#6-manuals)
 7.  [Examples](#7-examples)
 8.  [Real world use cases](#8-real-world-use-cases)
 9.  [Implementation note](#9-implementation-note)
 10. [FAQ](#10-faq)
 -   [Appendix](#appendix)


1 Introduction
--------------

The [FreeBSD](https://www.freebsd.org) kernel maintains a Management Information
Base ("MIB") where an object represents a parameter of the system. The _sysctl_
system call explores the MIB to find an object by its OID then calls its handler
to get or set the value of the parameter. The MIB is implemented by a collection 
of trees, the root nodes are the objects with level 1 and are entries of a
[RB](https://man.freebsd.org/tree/3)
(or [SLIST](https://man.freebsd.org/queue/3) if \_\_FreeBSD\_version < 1400071).
A node is defined by _struct sysctl\_oid_ and represents an object.
The complete MIB data structure is known as _sysctl MIB-Tree_ or _sysctl tree_.


It is often necessary to find an object not to call its handler but to get its
info (description, type, OID by name, next object, and so on), a typical example
is /sbin/sysctl :

	> sysctl -d kern.ostype
	kern.ostype: Operating system type
	> sysctl -t kern.ostype
	kern.ostype: string
	> sysctl -aN
	...


The kernel provides an undocumented interface (in _kern\_sysctl.c_) to explore 
the sysctl tree and to pass the info of an object to the userland, the purpose 
of _sysctlinfo_ is to provide a better interface. Obviously the interfaces can 
coexist, the tools and libraries can continue to use the kernel interface while 
the converted utilities can exploit the features of _sysctlinfo_.

2 Description
-------------

 - FreeBSD Quartely Status Report:
   [sysctlinfo](https://www.freebsd.org/status/report-2019-07-2019-09.html#sysctlinfo).
 - Paper and slides:
   [BSDCan 2020](https://www.bsdcan.org/events/bsdcan_2020/schedule/session/48-sysctlinfo-a-new-interface-to-visit-the-freebsd-sysctl-mib-and-to-pass-the-objects-info-to-userland/).

3 Getting started
-----------------

[portlink]: https://www.freshports.org/sysutils/sysctlinfo-kmod/
To install the port ([sysutils/sysctlinfo-kmod][portlink]):

	# cd /usr/ports/sysutils/sysctlinfo-kmod/ && make install clean

To add the package:

	# pkg install sysctlinfo-kmod

Example to implement */sbin/sysctl -aN*:
```c
#include <sys/param.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>
#include <sysctlinfo.h>

int main()
{
        char name[MAXPATHLEN], next[MAXPATHLEN];
        int prop[2] = {CTL_SYSCTL, NEXTOBJNODE_BYNAME};
        size_t nextlen = MAXPATHLEN;

        strcpy(next, "kern");
        do {
                strncpy(name, next, nextlen);
                printf("%s\n", name);
                nextlen = MAXPATHLEN;
        } while(SYSCTLINFO_BYNAME(name, prop, next, &nextlen) == 0);

        return (0);
}
```
```
% cc -I/usr/local/include example.c -o example
% ./example
```

4 Features
----------

_sysctlinfo_ implements a new set of object to provides an improved
version of the undocumented interface, moreover new features have been
implemented: to handle an OID up to CTL\_MAXNAME levels, to build a correct
OID also from a name with some empty string, capability mode for security
requirements, new features to get more info about an object and
to avoid extra computation in userland. New objects:
**sysctl.objid_byname**, **sysctl.objfakename**, **sysctl.objdesc**,
**sysctl.objlabel**, **sysctl.objkind**, **sysctl.objfmt**,
**sysctl.nextobjnode**, **sysctl.nextobjleaf**, **sysctl.objhashandler** and
**sysctl.fakenextobjleafnoskip**.

The new objects can convert utilities to use _sysctlinfo_ quickly, however 
the interface is still inefficient: it can pass a single info then the kernel 
needs to find the same objects many times 
(example [sysctlview](https://gitlab.com/alfix/sysctlview)), so
**sysctl.serobj**, **sysctl.serobjnextnode** and **sysctl.serobjnextleaf**
find the object in the MIB and pass all the info in one time.

Finally, _\*byname_ objects: **sysctl.objid_byname**,
**sysctl.objfakeid_byname**, **sysctl.objdesc_byname**,
**sysctl.objlabel_byname**, **sysctl.objkind_byname**, **sysctl.objfmt_byname**,
**sysctl.nextobjnode_byname**, **sysctl.nextobjleaf_byname**,
**sysctl.serobj_byname**, **sysctl.serobjnextnode_byname**,
**sysctl.serobjnextleaf_byname** and **sysctl.objhashandler_byname** search the
object by its name avoiding to call sysctl.name2oid (or similar) to explore the
MIB just to find the corresponding OID.

5 API
-----

sysctlinfo provides two main macros:
```c
int SYSCTLINFO(int *id, size_t idlevel, int prop[2], void *buf, size_t *buflen);
int SYSCTLINFO_BYNAME(char *name, int prop[2], void *buf, size_t *buflen);
```
The macros seek the object with _id/idlevel_ or _name_, then the information 
specified by _prop_ is copied into the buffer _buf_. Before the call _buflen_ 
gives the size of _buf_, after a successful call _buflen_ gives the amount of 
data copied; the size of the info can be determined with the NULL argument for 
_buf_, the size will be returned in the location pointed to by _buflen_. The 
value of _prop[0]_ should be CTL\_SYSCTL and _prop[1]_ can specify the 
desired info, the possible values are listed later.

_SYSCTLINFO_ and _SYSCTLINFO\_BYNAME_ return the value 0 if successful; 
otherwise the value -1 is returned and the global variable errno is set to 
indicate the error.

```c
#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdbool.h>
#include <sysctlinfo.h>

int prop[2], id[CTL_MAXNAME], *idp, idnext[CTL_MAXNAME], *idnextp, error;
size_t idlevel, idnextlevel, buflen;
char buf[ARBITRARY], name[MAXPATHLEN], *namep, *descp, *fmtp, *labelp;
unsigned int kind;
bool handler;

prop[0] = CTL_SYSCTL;
```

--------------------------------------------------------------------------------

**sysctl.objname**, object name by its OID

 - new feature

```c
prop[1] = OBJNAME;
error = SYSCTLINFO(id, idlevel, prop, NULL, &buflen);
error = SYSCTLINFO(id, idlevel, prop, buf,  &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objid\_byname**, object ID by its name

 - new feature

```c
prop[1] = OBJID_BYNAME;
error = SYSCTLINFO_BYNAME(name, prop, NULL, &idlevel);
error = SYSCTLINFO_BYNAME(name, prop, id,   &idlevel);
idlevel /= sizeof(int);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objfakename**, object (possible fake) name by its OID

 - corresponding to sysctl.name of kern\_sysctl.c
 - set buf and buflen like name and name-length of the node if it exists, 
   otherwise build a "fake" name depending on the id, example 
   [1.1.100.500.1000] -> "kern.ostype.100.500.1000" and 0 is returned
 - the kernel builds a fake level name up to 10 digits, 
   instead sysctlinfo builds a fake name up to SYSCTLINFO\_MAXFAKE - 1 digits

```c
prop[1] = OBJFAKENAME;
error = SYSCTLINFO(id, idlevel, prop, NULL, &buflen);
error = SYSCTLINFO(id, idlevel, prop, buf,  &buflen);
```

 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0
   or is not an integer

--------------------------------------------------------------------------------

**sysctl.objfakeid\_byname**, object (possible fake) ID by its name

 - corresponding to sysctl.name2oid of kern\_sysctl.c
 - Build an incomplete OID if some level is the empty string
 - Example: "security.jail.param.allow.mount." (note the last empty string) this
   object has 6 levels but objfakeid\_byname builds: 
   [2147482898.2147482984.2147482971.2147482939.147482928] deleting the last,
   6th,2147482927

```c
prop[1] = OBJFAKEID_BYNAME;
error = SYSCTLINFO_BYNAME(name, prop, NULL, &idlevel);
error = SYSCTLINFO_BYNAME(name, prop, id,   &idlevel);
idlevel /= sizeof(int);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objdesc**, object description by its OID

 - corresponding to sysctl.oiddescr of kern\_sysctl.c

```c
prop[1] = OBJDESC;
error = SYSCTLINFO(id, idlevel, prop, NULL, &buflen);
error = SYSCTLINFO(id, idlevel, prop, buf,  &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: description is NULL
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objdesc\_byname**, object description by its name

 - new feature

```c
prop[1] = OBJDESC_BYNAME;
error = SYSCTLINFO_BYNAME(name, prop, NULL, &buflen);
error = SYSCTLINFO_BYNAME(name, prop, buf,  &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: description is NULL
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objlabel**, object label by its OID

 - corresponding to {0.6} sysctl.oidlabel of kern\_sysctl.c

```c
prop[1] = OBJLABEL;
error = SYSCTLINFO(id, idlevel, prop, NULL, &buflen);
error = SYSCTLINFO(id, idlevel, prop, buf,  &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: label is NULL
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objlabel\_byname**, object label by its name

 - new feature

```c
prop[1] = OBJLABEL_BYNAME;
error = SYSCTLINFO_BYNAME(name, prop, NULL, &buflen);
error = SYSCTLINFO_BYNAME(name, prop, buf,  &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: label is NULL
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objkind**, object kind (flags and type) by its OID

 - new feature (sysctl.oidfmt of kern\_sysctl.c returns [kind|fmt])

```c
prop[1] = OBJKIND;
buflen = sizeof(unsigned int);
error = SYSCTLINFO(id, idlevel, prop, &kind, &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objkind\_byname**, object kind (flags and type) by its name

 - new feature

```c
prop[1] = OBJKIND_BYNAME;
buflen = sizeof(unsigned int);
error = SYSCTLINFO_BYNAME(name, prop, &kind, &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objfmt**, object format string by its OID

 - new feature (sysctl.oidfmt of kern\_sysctl.c returns [kind|fmt])

```c
prop[1] = OBJFMT;
error = SYSCTLINFO(id, idlevel, prop, NULL, &buflen);
error = SYSCTLINFO(id, idlevel, prop, buf,  &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: format string is NULL
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objfmt\_byname**, object format string by its name

 - new feature

```c
prop[1] = OBJFMT_BYNAME;
error = SYSCTLINFO_BYNAME(name, prop, NULL, &buflen);
error = SYSCTLINFO_BYNAME(name, prop, buf,  &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: format string is NULL
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.nextobjnode**, next node OID of a "Depth-First Traversal"

 - corresponding to sysctl.nextnoskip of kern\_sysctl.c
 - skips the nodes with CTLFLAG\_DORMANT 

```c
prop[1] = NEXTOBJNODE;
error = SYSCTLINFO(id, idlevel, prop, NULL,   &idnextlevel);
error = SYSCTLINFO(id, idlevel, prop, idnext, &idnextlevel);
idnextlevel /= sizeof(int);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: object has not a next node

--------------------------------------------------------------------------------

**sysctl.nextobjnode\_byname**, next node name of a "Depth-First Traversal"

 - new feature
 - skips the nodes with CTLFLAG\_DORMANT 

```c
prop[1] = NEXTOBJNODE_BYNAME;
error = SYSCTLINFO_BYNAME(name, prop, NULL, &buflen);
error = SYSCTLINFO_BYNAME(name, prop, buf,  &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: format string is NULL

--------------------------------------------------------------------------------

**sysctl.nextobjleaf**, next leaf OID of a "Depth-First Traversal"

 - new feature
 - skips CTLTYPE\_NODE with a NULL handler or CTLFLAG\_DORMANT flag
 
```c
prop[1] = NEXTOBJLEAF;
error = SYSCTLINFO(id, idlevel, prop, NULL,   &idnextlevel);
error = SYSCTLINFO(id, idlevel, prop, idnext, &idnextlevel);
idnextlevel /= sizeof(int);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: object has not a next leaf

--------------------------------------------------------------------------------

**sysctl.nextobjleaf_byname**, next leaf name of a "Depth-First Traversal"

 - new feature
 - skips CTLTYPE\_NODE with a NULL handler or CTLFLAG\_DORMANT flag

```c
prop[1] = NEXTOBJLEAF_BYNAME;
error = SYSCTLINFO_BYNAME(name, prop, NULL, &buflen);
error = SYSCTLINFO_BYNAME(name, prop, buf,  &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: object has not a newxt leaf

--------------------------------------------------------------------------------

**sysctl.serobj**, object serialization by its OID

 - new feature
 - if description, fmt or label is NULL, descp, fmtp or labep is set to '\0' 
   respectively

```c
prop[1] = SEROBJ;
error = SYSCTLINFO(id, idlevel, prop, NULL, &buflen);
error = SYSCTLINFO(id, idlevel, prop, buf,  &buflen);
SYSCTLINFO_DESOBJ(buf, idlevel, idp, namep, descp, kind, fmtp, labelp);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.serobj\_byname**, object serialization by its name

 - new feature
 - if description, fmt or label is NULL, descp, fmtp or labep is set to '\0' 
   respectively

```c
prop[1] = SEROBJ_BYNAME;
error = SYSCTLINFO_BYNAME(name, prop, NULL, &buflen);
error = SYSCTLINFO_BYNAME(name, prop, buf,  &buflen);
SYSCTLINFO_DESOBJ(buf, idlevel, idp, namep, descp, kind, fmtp, labelp);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.serobjnextnode**, object serialization with next node OID

 - new feature
 - skips nodes with CTLFLAG\_DORMANT
 - if description, fmt or label is NULL, descp, fmtp or labep is set to '\0' 
   respectively
 - if a next node does not exist idnextlevel is set to 0

```c
prop[1] = SEROBJNEXTNODE;
error = SYSCTLINFO(id, idlevel, prop, NULL, &buflen);
error = SYSCTLINFO(id, idlevel, prop, buf,  &buflen);
SYSCTLINFO_DESOBJ_NEXTOID(buf, idlevel, idp, namep, descp, kind, fmtp, labelp, idnextlevel, idnextp);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.serobjnextnode\_byname**, object serialization with next node name

 - new feature
 - skips nodes with CTLFLAG\_DORMANT
 - if description, fmt or label is NULL, descp, fmtp or labep is set to '\0' 
   respectively
 - if a next node does not exist nextnamep is set to '\0'

```c
prop[1] = SEROBJNEXTNODE_BYNAME;
error = SYSCTLINFO_BYNAME(name, prop, NULL, &buflen);
error = SYSCTLINFO_BYNAME(name, prop, buf,  &buflen);
SYSCTLINFO_DESOBJ_NEXTNAME(buf, idlevel, idp, namep, descp, kind, fmtp, labelp, nextnamep);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.serobjnextleaf**, object serialization with next leaf OID

 - new feature
 - skips CTLTYPE\_NODE with a NULL handler or CTLFLAG\_DORMANT flag
 - if description, fmt or label is NULL, descp, fmtp or labep is set to '\0' 
   respectively
 - if a next leaf does not exist idnextlevel is set to 0

```c
prop[1] = SEROBJNEXTLEAF;
error = SYSCTLINFO(id, idlevel, prop, NULL, &buflen);
error = SYSCTLINFO(id, idlevel, prop, buf,  &buflen);
SYSCTLINFO_DESOBJ_NEXTOID(buf, idlevel, idp, namep, descp, kind, fmtp, labelp, idnextlevel, idnextp);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.serobjnextleaf\_byname**, object serialization with next leaf name

 - new feature
 - skips CTLTYPE\_NODE with a NULL handler or CTLFLAG\_DORMANT flag
 - if description, fmt or label is NULL, descp, fmtp or labep is set to '\0' 
   respectively
 - if a next leaf does not exist nextnamep is set to '\0'

```c
prop[1] = SEROBJNEXTLEAF_BYNAME;
error = SYSCTLINFO_BYNAME(name, prop, NULL, &buflen);
error = SYSCTLINFO_BYNAME(name, prop, buf,  &buflen);
SYSCTLINFO_DESOBJ_NEXTNAME(buf, idlevel, idp, namep, descp, kind, fmtp, labelp, nextnamep);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objhashandler**, has the object a defined handler?

 - new feature
 - KASSERT(9) object has the CTLFLAG\_DYING flag

```c
prop[1] = OBJHASHANDLER;
buflen = sizeof(bool);
error = SYSCTLINFO(id, idlevel, prop, &handler, &buflen);
```

 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.objhashandler_byname**, has the object a defined handler?

 - new feature

```c
prop[1] = OBJHASHANDLER_BYNAME;
buflen = sizeof(bool);
error = SYSCTLINFO_BYNAME(name, prop, &handler, &buflen);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: name has more than CTL\_MAXNAME levels
 - [ENOENT] error: object does not exist
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode

--------------------------------------------------------------------------------

**sysctl.fakenextobjleafnokip**, next leaf OID of a "Depth-First Traversal",
avoiding a leaf with the SKIP flag

 - corresponding to sysctl.next of kern\_sysctl.c
 - skips CTLTYPE\_NODE with a NULL handler, CTLFLAG\_DORMANT or
   CTLFLAG\_SKIP flags
 
```c
prop[1] = FAKENEXTOBJLEAFNOSKIP;
error = SYSCTLINFO(id, idlevel, prop, NULL,   &idnextlevel);
error = SYSCTLINFO(id, idlevel, prop, idnext, &idnextlevel);
idnextlevel /= sizeof(int);
```

 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [EINVAL] error: idlevel is either greater than CTL\_MAXNAME, equal to 0 or 
   is not an integer
 - [ENOENT] error: object does not exist
 - [ENOATTR] error: object has not a next leaf

--------------------------------------------------------------------------------

6 Manuals
---------

The interface has 2 manuals:
[sysctlinfo(4) and sysctlinfo(3)](https://alfonsosiciliano.gitlab.io/posts/2019-10-02-manuals-sysctlinfo.html).

7 Examples
----------

The [examples](examples) are in the *Public Domain* to build new projects:

 - [singleinfo.c](examples/singleinfo.c) - Useful for getting one property
 - [allinfo.c](examples/allinfo.c) - For getting all the properties
 - [allinfobyname.c](examples/allinfobyname.c) - For getting all the properties (name instead OID)
 - [capability.c](examples/capability.c) - Capability mode example
 - [handler.c](examples/handler.c) - For getting handler info

8 Real world use cases
----------------------

 - [sysctlview](https://gitlab.com/alfix/sysctlview)
 - [nsysctl](https://gitlab.com/alfix/nsysctl)
 - [sysctlmibinfo2](https://gitlab.com/alfix/sysctlmibinfo2)
 - [sysctlbyname-improved](https://gitlab.com/alfix/sysctlbyname-improved)
 - [mixertui](https://gitlab.com/alfix/mixertui)
 - [sysctl-mib-html](https://gitlab.com/alfix/sysctl-mib-html)
 - [converted tools](converted/)


9 Implementation note
---------------------

The core of _sysctlinfo_ is the function _"sysctlinfo\_interface()"_,
it implements the handler for all of the objects of the interface.
_sysctlinfo_ uses nothing from kern\_sysctl.c so it can be added
anywhere in the kernel.

In "capability mode", properly after a
[cap\_enter(2)](https://man.freebsd.org/cap_enter/2) call,
_sysctlinfo_ checks if the object has the CTLFLAG\_CAPRD or CTLFLAG\_CAPWR
flag before to return its info.
sysctl.nextobjnode, sysctl.nextobjnode\_byname, sysctl.nextobjleaf and
sysctl.nextobjleaf\_byname ignore capability flags to traverse the tree also
in capability mode, sysctl.objfakename ignores capability flags for
compatibility with the kernel interface.

The sysctl MIB-tree is a critical section, while sysctlinfo\_interface() 
explores the MIB and passes the info to the userland an obejct cannot be 
added/deleted, unfortunaltely sysctl\_root\_handler\_locked() of kern\_sysctl.c
unlock before to call the handler of an object, correctly the objects of the 
kernel interface use SYSCTL\_RLOCK(tracker) to take the
[rmlock(9)](https://man.freebsd.org/rmlock/9) lock.
Solutions:
 - sysctlinfo.c uses sysctl\_wlock()/sysctl\_wunlock(), actually a read/
   shared lock is sufficient but the kernel hasn't this API, this file can be 
   easly merged with kern\_mib.c, this is also the solution in the kmod,
 - Merging sysctlinfo with kern\_sysctl.c it is possible to use
   SYSCTL\_RLOCK(tracker)/SYSCTL\_RUNLOCK(tracker).
 - Adding sysctl\_rlock()/sysctl\_runlock() to sysctl.h/kern\_sysctl.c,
   using a shared lock the interface is more efficient.

10 FAQ
------

<dl>
<dt>Why sysctlinfo?</dt>
<dd>I wrote sysctlinfo to improve my utilities.</dd>
<dt>Port/Package?</dt>
<dd>[sysutils/sysctlinfo-kmod](https://www.freshports.org/sysutils/sysctlinfo-kmod/).</dd>
<dt>Why the "obj" prefix?</dt>
<dd>Because the kernel has "oid" and a recent commit took the "CTL_SYSCTL_"
prefix for the undocumented interface.</dd>
<dt>Can idbyname handle a name extended for a CTLTYPE_NODE with a handler
(example "kern.proc.pid.&lt;pid&gt;")?</dt>
<dd>[sysctlbyname-improved](https://gitlab.com/alfix/sysctlbyname-improved)
provides sysctl.objidextended_byname to handle an expanded name.</dd>
<dt>Could we have a SNMP-OID compatible implementation instead of a tree-node-OID?</dt>
<dd>Maybe in the future.</dd>
<dt>Does exist a high level API on sysctlinfo?</dt>
<dd>[sysctlmibinfo2](https://gitlab.com/alfix/sysctlmibinfo2).</dd>
</dl>

Appendix
--------

**Interfaces comparision and improvements**

| kernel interface   | sysctlinfo                                              |
| ------------------ | ------------------------------------------------------- |
| sysctl.name2oid    | sysctl.objfakeid\_byname                                |
|                    | sysctl.objid\_byname                                    |
|                    | [sysctl.objidextended\_byname][si]                      |
| sysctl.name        | sysctl.objfakename                                      |
|                    | sysctl.objname                                          |
| sysctl.next        | sysctl.fakenextobjleafnoskip                            |
|                    | sysctl.nextobjleaf                                      |
|                    | sysctl.nextobjleaf\_byname                              |
| sysctl.nextnoskip  | sysctl.nextobjnode                                      |
|                    | sysctl.nextobjnode\_byname                              |
| sysctl.oidfmt      | _(divided into objkind and objfmt)_                     |
|                    | sysctl.objkind                                          |
|                    | sysctl.objkind\_byname                                  |
|                    | sysctl.objfmt                                           |
|                    | sysctl.objfmt\_byname                                   |
| sysctl.oiddescr    | sysctl.objdesc                                          |
|                    | sysctl.objdesc\_byname                                  |
| sysctl.oidlabel    | sysctl.objlabel                                         |
|                    | sysctl.objlabelbyname                                   |
|                    | sysctl.hashandler                                       |
|                    | sysctl.hashandler\_byname                               |
|                    | sysctl.serobj                                           |
|                    | sysctl.serobj\_byname                                   |
|                    | sysctl.serobj\_nextnode                                 |
|                    | sysctl.serobjnextnode\_byname                           |
|                    | sysctl.serobjnextleaf                                   |
|                    | sysctl.serobjnextleaf\_byname                           |

[si]: https://gitlab.com/alfix/sysctlbyname-improved

**CTL\_MAXNAME - 2 limit**

"CTL\_MAXNAME-2 limit": CTL\_MAXNAME (sys/sysctl.h) defines the max level to 24,
so [sysctl(9)](https://man.freebsd.org/sysctl/9) can add an object up to 24
levels, _testing/addnodes.c_ adds:
```
a1.a2.a3.a4.a5.a6.a7.a8.a9.a10.a11.a12.a13.a14.a15.a16.a17.a18.a19.a20.a21.a22
b1.b2.b3.b4.b5.b6.b7.b8.b9.b10.b11.b12.b13.b14.b15.b16.b17.b18.b19.b20.b21.b22.b23
c1.c2.c3.c4.c5.c6.c7.c8.c9.c10.c11.c12.c13.c14.c15.c16.c17.c18.c19.c20.c21.c22.c23.c24
```

[sysctl(3)](https://man.freebsd.org/sysctl/3) can get or set the value of a22,
b23 and c24.

Unfortunately the kernel undocumented interface can manage objects up to 
CTL\_MAXNAME - 2 levels. Consequently /sbin/sysctl fails with an object of 23 
or 24 levels 

```
% /sbin/sysctl a1
a1.a2.a3.a4.a5.a6.a7.a8.a9.a10.a11.a12.a13.a14.a15.a16.a17.a18.a19.a20.a21.a22: 22
% /sbin/sysctl b1
sysctl: sysctl(getnext) -1 88: Cannot allocate memory
% /sbin/sysctl b1.b2.b3.b4.b5.b6.b7.b8.b9.b10.b11.b12.b13.b14.b15.b16.b17.b18.b19.b20.b21.b22.b23
sysctl: sysctl fmt -1 1024 22: Invalid argument
```

/sbin/sysctl is not guilty, another example with
[prometheus_sysctl_exporter(8)](https://man.freebsd.org/prometheus_sysctl_exporter/8):
```
% /usr/sbin/prometheus_sysctl_exporter b1
prometheus_sysctl_exporter: sysctl(oidfmt): Invalid argument
% /usr/sbin/prometheus_sysctl_exporter b1.b2.b3.b4.b5.b6.b7.b8.b9.b10.b11.b12.b13.b14.b15.b16.b17.b18.b19.b20.b21.b22.b23
prometheus_sysctl_exporter: sysctl(oidfmt): Invalid argument
```

The kernel undocumented interface has 2 _patterns_, pseudocode:
```c
/* -1- {0,3} to get get id-by-name */
sysctl([0,3], 2, *id, &idlevel, name, namelen);

/* -2- {0,1-2-4-5-6} to get name, next, flags-type-fmt, desc and label by id */
memcpy([0,X] + 2, id, idlevel * sizeof(int));
sysctl([0,X,..id..], idlevel + 2, prop, &proplen, NULL, 0);
```

Note _idlevel + 2_ of the last sysctl(), _idlevel_ is the length of _id_, 
it could be correctly CTL\_MAXNAME=24 so _idlevel + 2_ is 26, but
26 > CTL\_MAXNAME then the second _pattern_ can manage an object up to 22 
levels (CTL\_MAXNAME - 2). Properly the second _pattern_ is for a
CTLTYPE\_NODE with a defined handler.

sysctlinfo can manage objects up to CTL\_MAXNAME levels, an example with
/sbin/sysctl converted to use _sysctlinfo_:

```
% converted/sysctl-sysctlinfo c1
c1.c2.c3.c4.c5.c6.c7.c8.c9.c10.c11.c12.c13.c14.c15.c16.c17.c18.c19.c20.c21.c22.c23.c24: 24
% sysctlinfo/converted/sysctl-sysctlinfo c1.c2.c3.c4.c5.c6.c7.c8.c9.c10.c11.c12.c13.c14.c15.c16.c17.c18.c19.c20.c21.c22.c23.c24
c1.c2.c3.c4.c5.c6.c7.c8.c9.c10.c11.c12.c13.c14.c15.c16.c17.c18.c19.c20.c21.c22.c23.c24: 24
```

**Incomplete OID**

sysctl(3) and sysctlbyname(3) could find different objects because
sysctlbyname(3) uses sysctl.name2oid to find the OID of an object by its name,
it can build an incomplete OID if the name has some level with an empty string,
example:
```
/* `nsysctl -pNy security.jail.param.allow.mount.' to get its OID */
int oid[6] = {2147482894, 2147482981, 2147482968, 2147482936, 2147482925, 2147482924};

if (sysctlbyname("security.jail.param.allow.mount.", buf, &buflen, NULL,0) != 0)
	printf("error byname\n");
if (sysctl(oid,6, buf, &buflen, NULL,0) != 0)
	printf("error\n");
```

sysctl.name2oid gets [2147482894, 2147482981, 2147482968, 2147482936, 2147482925]: 
5 levels not 6, so sysctlbyname() gets the value of the parent (in this case it 
returns -1 because the parent is not readable, instead sysctl() returns 0 
getting the value correctly). An exmple with /sbin/sysctl:

```
% /sbin/sysctl security.jail.param.allow.mount.
security.jail.param.allow.mount.tmpfs: 0
security.jail.param.allow.mount.debugfs: 0
security.jail.param.allow.mount.anon_inodefs: 0
security.jail.param.allow.mount.procfs: 0
security.jail.param.allow.mount.devfs: 0
security.jail.param.allow.mount.: 0
```

/sbin/sysctl prints the children of "security.jail.param.allow.mount".

sysctl converted on _sysctlinfo_ uses sysctl.objid\_byname printing only the
right object:
```
% converted/sysctl-sysctlinfo security.jail.param.allow.mount.
security.jail.param.allow.mount.: 0
```

Note 1, "security.jail.param.allow.mount." is not a pseudo/virtual object, it 
is a real *node* existing in the tree with a type, description, and so on, 
its name is simply '\0', so, considering
[kern\_sysctl.c](https://cgit.freebsd.org/src/log/sys/kern/kern_sysctl.c),
sysctl\_sysctl\_next\_ls() and sysctl\_sysctl\_name() find it, instead 
sysctl\_sysctl\_name2oid() gets the parent.

Note 2, Do not confuse with kern.proc.pid.\<pid\>, it is not a real node,
properly \`kern.proc.pid' is CTLTYPE\_NODE and \<pid\> is its arg1,
so (correctly) /sbin/sysctl shows only \`kern.proc.pid'.

**Extended name to OID**

sysctl.name2oid cannot convert an extended name for a CTLTYPE\_NODE with a
defined handler, example kern.proc.pid.\<pid\>, instead
sysctl.objidextended\_byname of
[sysctlbyname\_improved](https://gitlab.com/alfix/sysctlbyname-improved) can
convert an extended name.

**OID to name**

sysctl.name returns always zero (false positive) building a "fake" name 
depending on the input if the object does not exist, example 1.1.100.500.1000
-\> "kern.ostype.100.500.1000", sysctl.objfakename is equivalent instead
sysctl.objname returns an error.

Note 3, Do not confuse sysctl.name / sysctl.objfakename / sysctl.objname
(OID -\> name), with sysctl.name2oid / sysctl.objfakeid\_byname /
sysctl.objid\_byname (name -\> OID).

**Kind**

*struct sysctl\_oid* has: *oid\_kind* and *oid\_type*, sysctl.oidfmt joins them
and returns a void*  
|| u\_int \[flags and type\] | format-string '\0' ||  
sysctlinfo divides it into sysctl.objkind and sysctl.objfmt.

**Conclusion**

Finally, sysctlinfo provides capability mode, is efficient to get all object
info, has this README, manual pages and examples.

