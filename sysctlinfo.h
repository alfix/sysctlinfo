/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2019-2022 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _SYSCTLINFO_H_
#define _SYSCTLINFO_H_

#define SYSCTLINFO_VERSION 20221211

/* MIB level 1: kmod FreeBSD <= 13 */
#ifndef CTL_SYSCTL
#define CTL_SYSCTL		0
#endif

/* MIB level 2: sysctlinfo OIDs */
/* #define OBJIDEXTENDED_BYNAME	10 sysctlbyname_improved */
#define OBJNAME			11
#define OBJID_BYNAME		12
#define OBJFAKENAME		13
#define OBJFAKEID_BYNAME	14
#define OBJDESC			15
#define OBJDESC_BYNAME		16
#define OBJLABEL		17
#define OBJLABEL_BYNAME		18
#define OBJKIND			19
#define OBJKIND_BYNAME		20
#define OBJFMT			21
#define OBJFMT_BYNAME		22
#define NEXTOBJNODE		23
#define NEXTOBJNODE_BYNAME	24
#define NEXTOBJLEAF		25
#define NEXTOBJLEAF_BYNAME	26
#define SEROBJ			27
#define SEROBJ_BYNAME		28
#define SEROBJNEXTNODE		29
#define SEROBJNEXTNODE_BYNAME	30
#define SEROBJNEXTLEAF		31
#define SEROBJNEXTLEAF_BYNAME	32
#define OBJHASHANDLER		33
#define OBJHASHANDLER_BYNAME	34
#define FAKENEXTOBJLEAFNOSKIP	35 /* partial, no fake */

#define SYSCTLINFO_MAXFAKENAME	64

#ifndef KLD_MODULE
#define SYSCTLINFO(id, idlevel, prop, buf, buflen) \
	sysctl(prop, 2, buf, buflen, id, idlevel * sizeof(int))

#define SYSCTLINFO_BYNAME(name, prop, buf, buflen) \
	sysctl(prop, 2, buf, buflen, name, strlen(name) + 1)

#define SYSCTLINFO_DESOBJ(buf, idlevel, id, name, descr, kind, fmt, label) do { \
	idlevel = *((size_t *)buf);						\
	id = (int*) (((char*)buf) + sizeof(size_t));				\
	name = ((char *)buf) + (idlevel * sizeof(int) + sizeof(size_t));	\
	descr = strchr(name, '\0') + 1;						\
	kind = *((unsigned int *) (strchr(descr, '\0') + 1));			\
	fmt = strchr(descr, '\0') + 1 + sizeof(unsigned int);			\
	label = strchr(fmt, '\0') + 1;						\
} while (0)

#define SYSCTLINFO_DESOBJ_NEXTOID(buf, idlevel, id, name, descr, kind, fmt, label, idnextlevel, idnext) do { \
	SYSCTLINFO_DESOBJ(buf, idlevel, id, name, descr, kind, fmt, label);	\
	idnextlevel = *((size_t *) ((strchr(label, '\0') + 1)));		\
	idnext = (int*) (strchr(label, '\0') + 1 + sizeof(size_t) );		\
} while (0)

#define SYSCTLINFO_DESOBJ_NEXTNAME(buf, idlevel, id, name, descr, kind, fmt, label, namenext) do { \
	SYSCTLINFO_DESOBJ(buf, idlevel, id, name, descr, kind, fmt, label);	\
	namenext = strchr(label, '\0') + 1;					\
} while (0)
#endif  /* ifndef KLD_MODULE */

#endif  /* _SYSCTLINFO_H_ */
