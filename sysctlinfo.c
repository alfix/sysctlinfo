/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2019-2022 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* MODULE SECTION, remove before to merge with kern_mib.c */
#include <sys/param.h>
#include <sys/capsicum.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/proc.h>
#include <sys/sysctl.h>
#include <sys/systm.h>
#include <sys/ucred.h>

#include "sysctlinfo.h"

#define CAPABILITY_MODE	1

static struct sysctl_ctx_list clist;

#if __FreeBSD_version < 1400071
#define EMPTY_CHILDREN(children) SLIST_EMPTY(children)
#define FIRST_CHILD(children) SLIST_FIRST(children)
#define NEXT_CHILD(child) SLIST_NEXT(child, oid_link)
#define FOREACH_CHILD(child, list) SLIST_FOREACH(child, list, oid_link)
#else
#define EMPTY_CHILDREN(children) RB_EMPTY(children)
#define FIRST_CHILD(children) RB_MIN(sysctl_oid_list, children)
#define NEXT_CHILD(child) RB_NEXT(sysctl_oid_list, SYSCTL_PARENT(child)->oid_children, child)
#define FOREACH_CHILD(child, list) RB_FOREACH(child, sysctl_oid_list, list)
#endif
/* end MODULE SECTION */

/*
 * sysctlinfo interface
 */
#define SYSCTLINFO_F_BYNAME	0x8000
#define SYSCTLINFO_F_NODE	0x4000
#define SYSCTLINFO_F_LEAF	0x2000
#define SYSCTLINFO_F_NOCAP	0x1000
#define SYSCTLINFO_INFOMASK	0x0FFF /* CTL_AUTO_START 0x100 in sysctl.h */

/* Utils, the caller has to hold a lock */
static int
sysctlinfo_nextnode_locked(struct sysctl_oid *path[CTL_MAXNAME],
    int id[CTL_MAXNAME], size_t *level, int info)
{
	struct sysctl_oid_list *children;
	size_t l = *level;
	bool nextleaf = (info & SYSCTLINFO_F_LEAF) ? true : false;

	info = info & SYSCTLINFO_INFOMASK;

	for (;;) {
		children = SYSCTL_CHILDREN(path[l-1]);
		if (children != NULL && !EMPTY_CHILDREN(children)) { /* children */
			path[l] = FIRST_CHILD(children);
			l++;
		} else { /* no child */
			for (;;) {
				if (l <= 0)
					break;
				if (NEXT_CHILD(path[l-1]) != NULL) {
					/* brother */
					path[l-1] = NEXT_CHILD(path[l-1]);
					break;
				} else { /* no brother */
					l--;
				}
			}
		}

		if (l <= 0 || l > CTL_MAXNAME)
			return (ENOATTR);

		id[l-1]=path[l-1]->oid_number;
		if (path[l-1]->oid_kind & CTLFLAG_DORMANT)
			continue;

		if (info == FAKENEXTOBJLEAFNOSKIP &&
		    path[l-1]->oid_kind & CTLFLAG_SKIP)
			continue;

		if (nextleaf) {
			if ((path[l-1]->oid_kind & CTLTYPE) != CTLTYPE_NODE ||
			    path[l-1]->oid_handler !=NULL)
				break;
		} else {
			break;
		}
	}

	*level=l;
	return 0;
}

static int
sysctlinfo_outname_locked(struct sysctl_req *req, size_t idlevel,
    struct sysctl_oid *path[CTL_MAXNAME])
{
	int i, error;

	for (i=0; i < idlevel; i++) {
		if (i > 0)
			error = SYSCTL_OUT(req, ".", 1);
		if (!error) {
			error = SYSCTL_OUT(req, path[i]->oid_name,
			    strlen(path[i]->oid_name));
		}
		if (error)
			return error;
	}
	error = SYSCTL_OUT(req, "", 1);

	return error;
}

static int
sysctlinfo_serobj_locked(struct sysctl_req *req,
    struct sysctl_oid *path[CTL_MAXNAME], int id[CTL_MAXNAME], size_t idlevel,
    struct sysctl_oid *oid)
{
	int error;

	/* idlevel */
	if ((error = SYSCTL_OUT(req, &idlevel, sizeof (size_t))) != 0)
		return error;
	/* id */
	if ((error = SYSCTL_OUT(req, id, idlevel * sizeof (int))) != 0)
		return error;
	/* name */
	if((error = sysctlinfo_outname_locked(req, idlevel, path)) !=0)
		return error;
	/* desc */
	if (oid->oid_descr == NULL)
		error = SYSCTL_OUT(req, "", 1);
	else
		error = SYSCTL_OUT_STR(req, oid->oid_descr);
	if (error != 0)
		return error;
	/* kind */
	error = SYSCTL_OUT(req, &oid->oid_kind, sizeof(oid->oid_kind));
	if (error)
		return error;
	/* fmt */
	if (oid->oid_fmt == NULL)
		error = SYSCTL_OUT(req, "", 1);
	else
		error = SYSCTL_OUT_STR(req, oid->oid_fmt);
	if(error != 0)
		return error;
	/* label */
	if (oid->oid_label == NULL)
		error = SYSCTL_OUT(req, "", 1);
	else
		error = SYSCTL_OUT_STR(req, oid->oid_label);

	return error;
}

/* Interface implementation */
static int
sysctlinfo_interface(SYSCTL_HANDLER_ARGS)
{
	int error = 0, id[CTL_MAXNAME], indx = 0, i;
	size_t idlevel, info = arg2 & SYSCTLINFO_INFOMASK;
	struct sysctl_oid *oid, *path[CTL_MAXNAME];
	struct sysctl_oid_list *lsp = &sysctl__children;
	char buf[SYSCTLINFO_MAXFAKENAME], name_input[MAXPATHLEN], *name_inputp;
	bool byname, has_handler;

	if (req->newlen == 0)
		return (EINVAL);

	/* Get OID or name from userspace */
	byname = (arg2 & SYSCTLINFO_F_BYNAME) ? true : false;

	if (byname) {
		if (req->newlen >= MAXPATHLEN)
			return (ENAMETOOLONG);
		memset(name_input, 0, MAXPATHLEN);
		if ((error = SYSCTL_IN(req, name_input, req->newlen)) !=0)
			return (error);
		idlevel=1;
		for (i=0; i<req->newlen; i++) {
			if (name_input[i] == '.') {
				name_input[i] = '\0';
				if (name_input[i+1] == '\0' &&
				    info == OBJFAKEID_BYNAME)
					break;
				idlevel++;
				if (idlevel > CTL_MAXNAME)
					return (EINVAL);
			}
		}
		name_inputp = &name_input[0];
	} else {
		if (req->newlen > CTL_MAXNAME * sizeof(int))
			return (EINVAL);
		if (req->newlen % sizeof(int) != 0)
			return (EINVAL);
		if ((error = SYSCTL_IN(req, id, req->newlen)) !=0)
			return (error);

		idlevel = req->newlen / sizeof(int);
	}

	/*
	 * The MIB-Tree is a critical section, sysctl() gets the lock but
	 * sysctl_root_handler_locked() unlock before to call this handler,
	 * a shared/rm_lock is sufficient but kern_sysctl.c has not this API
	 */
	sysctl_wlock();

	/* Find object */
	while (indx < idlevel) {
		FOREACH_CHILD(oid, lsp) {
			if (byname) {
				if (strcmp(name_inputp, oid->oid_name) == 0)
					break;
			} else {
				if (oid->oid_number == id[indx])
					break;
			}
		}
		if (oid == NULL) {
			break;
		}
		path[indx] = oid;
		if (byname)
			id[indx] = oid->oid_number;
		indx++;

		if (indx == idlevel) {
			KASSERT((oid->oid_kind & CTLFLAG_DYING) == 0,
			    ("%s found DYING node %p", __func__, oid));

			if ((oid->oid_kind & CTLTYPE) != CTLTYPE_NODE &&
			    (oid->oid_kind & CTLFLAG_DORMANT) != 0) {
				error = ENOENT;
				goto out;
			}
		} else { /* indx < idlevel */
			if (byname)
				name_inputp += (strlen(name_inputp) + 1);
			if ((oid->oid_kind & CTLTYPE) == CTLTYPE_NODE &&
			    oid->oid_handler == NULL) {
				lsp = SYSCTL_CHILDREN(oid);
			} else {
				break;
			}
		}
	}

	if (indx != idlevel && info != OBJFAKENAME) {
		error = ENOENT;
		goto out;
	}

	/* Capability check */
#ifdef CAPABILITY_MODE
	if (IN_CAPABILITY_MODE(req->td)) {
		if (!(oid->oid_kind & CTLFLAG_CAPRD) &&
		    !(oid->oid_kind & CTLFLAG_CAPWR) &&
		    !(arg2 & SYSCTLINFO_F_NOCAP)) {
			error = ECAPMODE;
			goto out;
		}
	}
#endif

	/* Pass info to userspace */
	switch (info) {
	case OBJID_BYNAME:
	case OBJFAKEID_BYNAME:
		error = SYSCTL_OUT(req, id, idlevel * sizeof (int));
		break;
	case OBJNAME:
		error = sysctlinfo_outname_locked(req, idlevel, path);
		break;
	case OBJFAKENAME:
		for (i=0; i < idlevel; i++) {
			if (i > 0)
				if ((error = SYSCTL_OUT(req, ".", 1)) != 0)
					goto out;

			if (i < indx) {
				error = SYSCTL_OUT(req, path[i]->oid_name,
				    strlen(path[i]->oid_name));
			} else {
				snprintf(buf, sizeof(buf), "%d", id[i]);
				error = SYSCTL_OUT(req, buf, strlen(buf));
			}
			if (error)
				goto out;
		}
		error = SYSCTL_OUT(req, "", 1);
		break;
	case OBJDESC:
	case OBJDESC_BYNAME:
		if (oid->oid_descr == NULL)
			error = ENOATTR;
		else
			error = SYSCTL_OUT_STR(req, oid->oid_descr);
		break;
	case OBJKIND:
	case OBJKIND_BYNAME:
		error = SYSCTL_OUT(req, &oid->oid_kind, sizeof(oid->oid_kind));
		break;
	case OBJFMT:
	case OBJFMT_BYNAME:
		if (oid->oid_fmt == NULL)
			error = ENOATTR;
		else
			error = SYSCTL_OUT_STR(req, oid->oid_fmt);
		break;
	case OBJLABEL:
	case OBJLABEL_BYNAME:
		if (oid->oid_label == NULL)
			error = ENOATTR;
		else
			error = SYSCTL_OUT_STR(req, oid->oid_label);
		break;
	case NEXTOBJNODE:
	case NEXTOBJLEAF:
	case FAKENEXTOBJLEAFNOSKIP:
		error = sysctlinfo_nextnode_locked(path, id, &idlevel, arg2);
		if (error != 0)
			break;
		error = SYSCTL_OUT(req, id, idlevel * sizeof (int));
		break;
	case NEXTOBJNODE_BYNAME:
	case NEXTOBJLEAF_BYNAME:
		error = sysctlinfo_nextnode_locked(path, id, &idlevel, arg2);
		if (error != 0)
			break;
		error = sysctlinfo_outname_locked(req, idlevel, path);
		break;
	case SEROBJ:
	case SEROBJ_BYNAME:
		error = sysctlinfo_serobj_locked(req, path, id, idlevel, oid);
		break;
	case SEROBJNEXTNODE:
	case SEROBJNEXTLEAF:
		error = sysctlinfo_serobj_locked(req, path, id, idlevel, oid);
		if (error != 0)
			break;
		error = sysctlinfo_nextnode_locked(path, id, &idlevel, arg2);
		if (error != 0)
			idlevel=0;
		if((error = SYSCTL_OUT(req, &idlevel, sizeof (size_t))) != 0)
			break;
		error = SYSCTL_OUT(req, id, idlevel * sizeof (int));
		break;
	case SEROBJNEXTNODE_BYNAME:
	case SEROBJNEXTLEAF_BYNAME:
		error = sysctlinfo_serobj_locked(req, path, id, idlevel, oid);
		if (error != 0)
			break;
		error = sysctlinfo_nextnode_locked(path, id, &idlevel, arg2);
		if (error != 0)
			idlevel=0;
		error = sysctlinfo_outname_locked(req, idlevel, path);
		break;
	case OBJHASHANDLER:
	case OBJHASHANDLER_BYNAME:
		has_handler = oid->oid_handler != NULL;
		error = SYSCTL_OUT(req, &has_handler, sizeof (bool));
		break;
	default:
		error = EOPNOTSUPP;
	}

out:
	sysctl_wunlock();
	return (error);
}

/* Kernel Module */
static int
sysctlinfo_modevent(module_t mod __unused, int event, void *arg __unused)
{
	int error = 0;

	switch (event) {
	case MOD_LOAD:
		sysctl_ctx_init(&clist);

#define	ADDSYSCTL(OID, name, flags, desc)				\
	SYSCTL_PROC(_sysctl, OID, name, CTLTYPE_OPAQUE | CTLFLAG_RW |	\
	    CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW, NULL,	\
	    flags | OID, sysctlinfo_interface, "MIB", desc)

		ADDSYSCTL(OBJNAME, objname, 0, "Object name by its OID");
		ADDSYSCTL(OBJID_BYNAME, objid_byname, SYSCTLINFO_F_BYNAME,
		    "Object ID by its name");
		ADDSYSCTL(OBJFAKENAME, objfakename, SYSCTLINFO_F_NOCAP,
		    "Object (possible fake) name");
		ADDSYSCTL(OBJFAKEID_BYNAME, objfakeid_byname, SYSCTLINFO_F_BYNAME,
		    "Object (possible fake) ID by its name");
		ADDSYSCTL(OBJDESC, objdesc, 0, "Object description string");
		ADDSYSCTL(OBJDESC_BYNAME, objdesc_byname, SYSCTLINFO_F_BYNAME,
		    "Object description by its name");
		ADDSYSCTL(OBJLABEL, objlabel, 0, "Object label");
		ADDSYSCTL(OBJLABEL_BYNAME, objlabel_byname, SYSCTLINFO_F_BYNAME,
		    "Object label by its name");
		ADDSYSCTL(OBJKIND, objkind, 0, "Object kind (flags and type)");
		ADDSYSCTL(OBJKIND_BYNAME, objkind_byname, SYSCTLINFO_F_BYNAME,
		    "Object kind by its name");
		ADDSYSCTL(OBJFMT, objfmt, 0, "Object format string");
		ADDSYSCTL(OBJFMT_BYNAME, objfmt_byname, SYSCTLINFO_F_BYNAME,
		    "object format string by its name");
		ADDSYSCTL(NEXTOBJNODE, nextobjnode,
		    (SYSCTLINFO_F_NODE | SYSCTLINFO_F_NOCAP),
		    "Next object (internal node or leaf)");
		ADDSYSCTL(NEXTOBJNODE_BYNAME, nextobjnode_byname,
		    (SYSCTLINFO_F_NODE | SYSCTLINFO_F_BYNAME | SYSCTLINFO_F_NOCAP),
		    "Next object name (internal node or leaf)");
		ADDSYSCTL(NEXTOBJLEAF, nextobjleaf,
		    (SYSCTLINFO_F_LEAF | SYSCTLINFO_F_NOCAP),
		    "Next object (only leaf)");
		ADDSYSCTL(NEXTOBJLEAF_BYNAME, nextobjleaf_byname,
		    (SYSCTLINFO_F_LEAF | SYSCTLINFO_F_BYNAME | SYSCTLINFO_F_NOCAP),
		    "Next object name (only leaf)");
		ADDSYSCTL(SEROBJ, serobj, 0, "Serialize object properties");
		ADDSYSCTL(SEROBJ_BYNAME, serobj_byname, SYSCTLINFO_F_BYNAME,
		    "Serialize object by its name");
		ADDSYSCTL(SEROBJNEXTNODE, serobjnextnode, SYSCTLINFO_F_NODE,
		    "Serialize object with next-node-OID");
		ADDSYSCTL(SEROBJNEXTNODE_BYNAME, serobjnextnode_byname,
		    (SYSCTLINFO_F_NODE | SYSCTLINFO_F_BYNAME),
		    "Serialize object by its name with next-node-name");
		ADDSYSCTL(SEROBJNEXTLEAF, serobjnextleaf, SYSCTLINFO_F_LEAF,
		    "Serialize object with next-leaf-OID");
		ADDSYSCTL(SEROBJNEXTLEAF_BYNAME, serobjnextleaf_byname,
		    (SYSCTLINFO_F_LEAF | SYSCTLINFO_F_BYNAME),
		    "Serialize object by its name with next-leaf-name");
		ADDSYSCTL(OBJHASHANDLER, objhashandler, 0, "Object has a handler");
		ADDSYSCTL(OBJHASHANDLER_BYNAME, objhashandler_byname,
		    SYSCTLINFO_F_BYNAME, "Object has a handler by its name");
		ADDSYSCTL(FAKENEXTOBJLEAFNOSKIP, fakenextobjleafnoskip,
		    SYSCTLINFO_F_LEAF, "Next object (only leaf) avoiding SKIP flag");
		break;
	case MOD_UNLOAD:
		if (sysctl_ctx_free(&clist)) {
			printf("sysctl_ctx_free failed.\n");
			return (ENOTEMPTY);
		}
		break;
	default:
		error = EOPNOTSUPP;
		break;
	}
	return (error);
}
static moduledata_t sysctlinfo_mod = {
	"sysctlinfo",
	sysctlinfo_modevent,
	NULL
};

DECLARE_MODULE(sysctlinfo, sysctlinfo_mod, SI_SUB_EXEC, SI_ORDER_ANY);
