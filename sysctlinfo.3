.\"
.\" Copyright (c) 2019-2022 Alfonso Sabato Siciliano
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.Dd April 20, 2022
.Dt SYSCTLINFO 3
.Os
.Sh NAME
.Nm SYSCTLINFO ,
.Nm SYSCTLINFO_BYNAME ,
.Nm SYSCTLINFO_DESOBJ ,
.Nm SYSCTLINFO_DESOBJ_NEXTOID ,
.Nm SYSCTLINFO_DESOBJ_NEXTNAME
.Nd sysctl MIB-Tree interface
.Sh SYNOPSIS
.In sys/types.h
.In sys/sysctl.h
.In sysctlinfo.h
.Fd #define OBJNAME
.Fd #define OBJID_BYNAME
.Fd #define FAKEOBJNAME
.Fd #define FAKEOBJID_BYNAME
.Fd #define OBJDESC
.Fd #define OBJDESC_BYNAME
.Fd #define OBJLABEL
.Fd #define OBJLABEL_BYNAME
.Fd #define OBJKIND
.Fd #define OBJKIND_BYNAME
.Fd #define OBJFMT
.Fd #define OBJFMT_BYNAME
.Fd #define NEXTOBJNODE
.Fd #define NEXTOBJNODE_BYNAME
.Fd #define NEXTOBJLEAF
.Fd #define NEXTOBJLEAF_BYNAME
.Fd #define SEROBJ
.Fd #define SEROBJ_BYNAME
.Fd #define SEROBJNEXTNODE
.Fd #define SEROBJNEXTNODE_BYNAME
.Fd #define SEROBJNEXTLEAF
.Fd #define SEROBJNEXTLEAF_BYNAME
.Fd #define OBJHASHANDLER
.Fd #define OBJHASHANDLER_BYNAME
.Fd #define NEXTOBJLEAFAVOIDSKIP
.Ft "int"
.Fo SYSCTLINFO
.Fa "int *id"
.Fa "size_t idlevel"
.Fa "int prop[2]"
.Fa "void *buf"
.Fa "size_t *buflen"
.Fc
.Ft "int"
.Fn SYSCTLINFO_BYNAME "char *name" "int prop[2]" "void *buf" "size_t *buflen"
.Ft "void"
.Fo SYSCTLINFO_DESOBJ
.Fa "void *buf"
.Fa "size_t idlevel"
.Fa "int *id"
.Fa "char *namep"
.Fa "char *descrp"
.Fa "unsigned int kind"
.Fa "char *fmtp"
.Fa "char *labelp"
.Fc
.Ft "void"
.Fo SYSCTLINFO_DESOBJ_NEXTOID
.Fa "void *buf"
.Fa "size_t idlevel"
.Fa "int *id"
.Fa "char *namep"
.Fa "char *descrp"
.Fa "unsigned int kind"
.Fa "char *fmtp"
.Fa "char *labelp"
.Fa "size_t idnextlevel"
.Fa "int *idnext"
.Fc
.Ft "void"
.Fo SYSCTLINFO_DESOBJ_NEXTNAME
.Fa "void *buf"
.Fa "size_t idlevel"
.Fa "int *id"
.Fa "char *namep"
.Fa "char *descrp"
.Fa "unsigned int kind"
.Fa "char *fmtp"
.Fa "char *labelp"
.Fa "char *namenextp"
.Fc
.Sh DESCRIPTION
Macros to wrap the
.Xr sysctlinfo 4
interface for exploring the sysctl MIB-Tree and for getting the properties of an
object, as it is not designed to get and set object values, anyone wishing to do
this should see
.Xr sysctl 3 .
.Pp
An object is identified by an Object Identifier
.Pq OID ,
a series of numbers, represented by a pair
.Fa "int *id"
and
.Fa "size_t idlevel" ,
the level should be between 1 and
.Dv CTL_MAXNAME .
It is possible to replace a number with a string to obtain an object name, e.g.,
[1.1] \(->
.Dq kern.ostype .
.Pp
.Fn SYSCTLINFO
and
.Fn SYSCLINFO_BYNAME
seek the node with
.Fa id
/
.Fa idlevel
or
.Fa name ,
then the information specified by
.Fa prop
is copied into the buffer
.Fa buf .
Before the call
.Fa buflen
gives the size of
.Fa buf ,
after a successful call
.Fa buflen
gives the amount of data copied; the size of the info can be determined with the
NULL argument for
.Fa buf ,
the size will be returned in the location pointed to by
.Fa buflen .
The value of
.Fa prop[0]
should be
.Dv CTL_SYSCTL
and
.Fa prop[1]
can specify the desired info, the possible values are listed later.
.Pp
.Fn SYSCTLINFO
accepts an array
.Fa id
of
.Fa idlevel
elements and the following
.Fa prop[1] :
.Bl -tag -width indent
.It Dv OBJFAKENAME
if the object exists
.Fa buf
is set like a string with its name, otherwise
.Fn SYSCTLINFO
build a name depending on the
.Fa id ,
e.g., with a non-existent OID
[1.1.100.500.1000] the name is
.Dq kern.ostype.100.500.1000 .
.It Dv OBJNAME , Dv OBJDESC , Dv OBJLABEL and Dv OBJFMT
set
.Fa buf
like a string with the name, description, label and format, respectively.
.It Dv OBJKIND
sets
.Fa buf
like an unsigned int with the kind of the object, its format is: 3 bytes for
flags and 1 byte for type, they are defined in
.In sys/sysctl.h .
.It HASHANDLER
sets
.Fa buf
like a bool value,
.Dv true
if the object has a defined handler,
.Dv false
otherwise.
.It Dv NEXTOBJLEAF and Dv NEXTOBJNODE
set
.Fa buf
like an integer array with the OID of the next leaf or also internal visited in
a
.Dq Depth-First Traversal ,
the next level is
.Dq buflen / sizeof(int)
to be consistent with the acceptable level range.
.It FAKENEXTOBJLEAFNOSKIP
sets
.Fa buf
like the OID of the next leaf ignoring the objects with the
.Dv SKIP
flag; this feature exists for compatibility with the undocumented interface, see
.Sx IMPLEMENTATION NOTES .
.It Dv DESOBJ
serializes the object info in
.Fa buf ,
it should be deserialized via
.Fn SYSCTLINFO_DESOBJ ,
if description, format or label is NULL, descp, fmtp or labep is set to
.Dq
respectively.
.It Dv SEROBJNEXTNODE and Dv SEROBJNEXTLEAF
serialize the object info like
.Dv DESOBJ
adding the next Object OID,
.Fa buf
should be deserialized via
.Fn SYSCTLINFO_DESOBJ_NEXTOID ,
if no next object exists
.Fa idnextlevel
is set to 0.
.El
.Pp
.Fn SYSCTLINFO_BYNAME
accepts a string
.Fa name
and the following
.Fa prop[1] :
.Bl -tag -width indent
.It Dv OBJFAKEID_BYNAME and Dv OBJID_BYNAME
set
.Fa buf
like an integer array with the OID of the object, the level is
.Dq buflen / sizeof(int) .
OBJFAKEID_BYNAME builds an incomplete OID if some level name is
.Dq ,
e.g., name
.Dq n1.n2.n3.
\(-> [1.2.3].
.It Dv OBJDESC_BYNAME , Dv OBJLABEL_BYNAME and Dv OBJFMT_BYNAME
set
.Fa buf
like a string with the description, label and format respectively.
.It Dv OBJKIND_BYNAME
sets
.Fa buf
like an unsigned int with the kind of the object, its format is: 3 bytes for
flags and 1 byte for type, they are defined in
.In sys/sysctl.h .
.It HASHANDLER_BYNAME
sets
.Fa buf
like a bool value,
.Dv true
if the object has a defined handler,
.Dv false
otherwise.
.It Dv NEXTOBJLEAF_BYNAME and Dv NEXTOBJNODE_BYNAME
set
.Fa buf
copy the name of the next leaf or also internal node visited in a
.Dq Depth-First Traversal .
.It Dv DESOBJ_BYNAME
serializes the object info in
.Fa buf ,
it should be deserialized via
.Fn SYSCTLINFO_DESOBJ ,
if description, format or label is NULL, descp, fmtp or labep is set to
.Dq
respectively.
.It Dv SEROBJNEXTLEAF_BYNAME and Dv SEROBJNEXTNODE_BYNAME
serialize the object info like
.Dv DESOBJ
adding the next object name,
.Fa buf
should be deserialized via
.Fn SYSCTLINFO_DESOBJ_NEXTNAME ,
if no next object exists
.Fa nextnamep
is set to
.Dq .
.El
.Sh IMPLEMENTATION NOTES
The kernel provides an undocumented interface for the info of the sysctl tree,
.Nm sysctlinfo
and the current kernel interface can coexist.
.Pp
In
.Dq capability mode ,
see
.Xr cap_enter 2 ,
.Nm sysctlinfo
checks if the object has
.Dv CTLFLAG_CAPRD
or
.Dv CTLFLAG_CAPWR
before to return its info.
.Dv NEXTOBJNODE ,
.Dv NEXTOBJLEAF ,
.Dv NEXTOBJNODE_BYNAME
and
.Dv NEXTOBJLEAF_BYNAME
ignore capability flags to traverse the tree also in capability mode,
.Dv OBJFAKENAME
ignores capability flags for compatibility with the kernel interface.
.Sh RETURN VALUES
.Rv -std SYSCTLINFO SYSCTLINFO_BYNAME
.Sh EXAMPLES
If installed:
.Dl /usr/local/share/examples/sysctlinfo/
.Pp
Example to explore the MIB printing only names
.Pp
.Bd -literal -offset indent -compact
char name[MAXPATHLEN], next[MAXPATHLEN];
int prop[2] = {CTL_SYSCTL, NEXTOBJNODE_BYNAME};
size_t nextlen = MAXPATHLEN;

strcpy(next, "sysctl");
do {
        strncpy(name, next, nextlen);
        printf("%s\n", name);
        nextlen = MAXPATHLEN;
} while(SYSCTLINFO_BYNAME(name, prop, next, &nextlen) == 0);
.Ed
.Pp
Example to explore the MIB printing all of the properties:
.Pp
.Bd -literal -offset indent -compact
int i, id[CTL_MAXNAME], *idp_unused, *idnextp;
size_t idlevel, idnextlevel, buflen, lev_unused;
unsigned int kind;
char buf[BUFSIZE], *namep, *descrp, *fmtp, *labelp;
int prop[2] = {CTL_SYSCTL, SEROBJNEXTNODE};

id[0]   = 0;
idlevel = 1;

for (;;) {
	buflen = BUFSIZE;
	if(SYSCTLINFO(id, idlevel, prop, buf, &buflen) != 0)
		err(1, "SEROBJNEXTNODE");

	SYSCTLINFO_DESOBJ_NEXTOID(buf, lev_unused, idp_unused, namep,
	    descrp, kind, fmtp, labelp, idnextlevel, idnextp);

	for (i = 0; i < idlevel; i++)
		printf("%d%c", id[i], i+1 < idlevel ? '.' : '\\n');
	printf("name:  %s\\n", namep);
	printf("descr: %s\\n", descrp);
	printf("label: %s\\n", labelp);
	printf("fmt:   %s\\n", fmtp);
	printf("kind:  %u\\n", kind);
	printf("flags: %u\\n", kind & 0xfffffff0);
	printf("type:  %u\\n", kind & CTLTYPE);
	printf("------------------------------------\\n");

	if (idnextlevel < 1)
		break;

	memcpy(id, idnextp, idnextlevel * sizeof(int));
	idlevel = idnextlevel;
}
.Ed
.Sh ERRORS
The following errors may be reported:
.Bl -tag -width Er
.It Bq Er ECAPMODE
In capability mode the object has not the CTLFLAG_CAPRD or CTLFLAG_CAPWR flag.
.It Bq Er EINVAL
.Fa name
has more than CTL_MAXNAME levels.
.It Bq Er EINVAL
.Fa idlevel
is either greater CTL_MAXNAME, equal to zero or is not an integer.
.It Bq Er ENAMETOOLONG
.Fa name
is >= MAXPATHLEN.
.It Bq Er ENOATTR
The object exists but its info is NULL.
.It Bq Er ENOENT
The object does not exist.
.El
.Sh SEE ALSO
.Xr cap_enter 2 ,
.Xr sysctl 3 ,
.Xr sysctlinfo 4
.Sh AUTHORS
.Nm sysctlinfo
was written by
.An Alfonso Sabato Siciliano
.Aq Mt asiciliano@FreeBSD.org .
